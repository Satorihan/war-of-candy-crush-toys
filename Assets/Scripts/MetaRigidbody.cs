﻿//Эта штуковена будет корректно работать только если на каждом подэлементе с коллайдером есть Rigidbody. Иначе не выйдет.
using System;
using UnityEngine;
//using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class MetaRigidbody : MonoBehaviour {

	public bool manualGeneration=false;
	public List<Rigidbody> rigparts;
	public Action<Collision> metacollisionEnterAction;
	Rigidbody rig2;
	int k;
	// Use this for initialization
	void Start () {
		Reinit ();
	}
	public void Reinit(){
		if (!manualGeneration){
			rigparts = new List<Rigidbody>();
			GetRigAndGoDeeper (transform);
		}
		for (k=0; k<rigparts.Count; k++) {
			rig2 = rigparts[k];
			if (rig2.gameObject.GetComponent<MetarigPart> () == null)
				rig2.gameObject.AddComponent<MetarigPart> ();
		}

		/*foreach (Rigidbody rig in rigparts) {
			if(rig.gameObject.GetComponent<MetarigPart>()==null)rig.gameObject.AddComponent<MetarigPart>();
		}*/
	}
	void GetRigAndGoDeeper(Transform choppa){
		Rigidbody temp;
		temp = choppa.GetComponent<Rigidbody> ();
		if (temp != null && !rigparts.Contains(temp)) {
			rigparts.Add(temp);
		}
		if (choppa.childCount > 0) {
			for (int i=0; i<choppa.childCount; i++) {
				GetRigAndGoDeeper(choppa.GetChild(i));
			}
		}
	}
	// Update is called once per frame
	/*
	void Update () {
	
	}
	*/
	public static MetaRigidbody GetMetarig(Transform target){
		MetaRigidbody temp;
		temp = target.GetComponent<MetaRigidbody> ();
		if (temp != null) {
			return temp;
		} else if(target.parent!=null){
			return GetMetarig(target.parent);
		}
		return null;
	}
	public static MetaRigidbody GetMetarig(GameObject target){
		return GetMetarig (target.transform);
	}
	public void ModifyRigidbody(Action<Rigidbody> todo){
		foreach (Rigidbody rig in rigparts) {
			todo(rig);
		}
	}
	public void OnMetacollisionEnter(Collision col){
		if(metacollisionEnterAction!=null)metacollisionEnterAction (col);
		//if (test != null)	test (col);
		//print ("Metacol!");
	}
	public bool isKinematic(bool state){
		foreach (Rigidbody rig in rigparts) {
			rig.isKinematic=state;
		}
		return state;
	}
}
