﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaisedBarbie : MonoBehaviour {
	Raiseable riser;
	MetaRigidbody metarig;
	Animator anim;
	bool isAlive;
	float cycleTime;
	public float hitDmg=2;
	public float hitRange=3;
	float eHitRng;
	public float hitPwr=30;
	//List <GameObject> targets = new List<GameObject> ();
	GameObject target;
	//List <float> distance = new List<float> ();
	float distToTarg;
	NavMeshAgent agent;
	public Transform hips;
	Vector3 tempTargPos;
	float tempDist;
	// Use this for initialization
	void Awake(){
		riser = GetComponent<Raiseable> ();
		metarig = GetComponent<MetaRigidbody> ();
		anim = /*transform.GetChild(0).*/GetComponent<Animator> ();
		agent = GetComponent<NavMeshAgent> ();
		isAlive = false;
	}
	void Start () {
		anim.enabled = false;
		agent.enabled = false;
		//agent.Stop ();
		riser.MakeAlive = LifeStart;
		riser.UnmakeAlive = LifeEnd;
		riser.getupTime = 3;
		cycleTime = riser.cycleTime;
		eHitRng = hitRange * hitRange;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (transform.position);
	
	}
	Vector3 tempv,tempv2;
	float animLength;
	void LifeStart(){
		isAlive = true;
		metarig.isKinematic (true);
		anim.enabled = true;

		//transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y,0);
		//определить направление торса, тут нам поможет измерение поворота по Х. Х>0 - вниз, иначе вверх.
		transform.position = new Vector3 (hips.position.x, 0.734f, hips.position.z);
		if (transform.rotation.eulerAngles.x < 180) {
			anim.Play ("barbie_get_up1");
			transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y-transform.rotation.eulerAngles.z,0);
			animLength=4.167f;
		} else {
			anim.Play ("barbie_getup01");
			animLength=3.25f;
			transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y+transform.rotation.eulerAngles.z,0);
		}
		//transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y,0);
		Invoke ("ReadyForAction", animLength);
	}

	void ReadyForAction(){
		agent.enabled = true;
		//anim.applyRootMotion = false;
		StartCoroutine (SearchAndDestroy());
	}
	GameObject GetTarget(){
		target = null;
		//distToTarg = 0;
		for (int i=0; i<interfaceControl.Instance.enemies.Count; i++) {
			tempTargPos=interfaceControl.Instance.enemies[i].transform.position;
			if(tempTargPos.z<0&&tempTargPos.x>-27&&tempTargPos.x<13){
				tempDist=(tempTargPos-transform.position).sqrMagnitude;
				if(target==null||tempDist<distToTarg){
					target=interfaceControl.Instance.enemies[i];
					distToTarg=tempDist;
				}
			}
		}
		return target;
	}
	IEnumerator SearchAndDestroy(){
		while (isAlive) {
			/*
			targets.Clear();
			distance.Clear();
			targets.AddRange(GameObject.FindGameObjectsWithTag("enemy"));
			for (int i=0; i<targets.Count; i++) {
				if(targets[i].activeSelf==false){
					targets.RemoveAt(i);
					i--;
				}
			}
			if(targets.Count==0){//Врагов нет
			*/
			GetTarget();
			if(target==null){
				//print("Sector clear");
				//agent.SetDestination(transform.position);
				agent.ResetPath();
				anim.SetBool("Run",false);
			}
			else {//Враги таки есть
				//print("Enemy spotted");
				/*
				for(int i=0;i<targets.Count;i++){
					distance.Add((transform.position-targets[i].transform.position).magnitude);
				}
				target=targets[0];
				distToTarg=distance[0];
				for(int i=1;i<distance.Count;i++){
					if(distance[i]<distToTarg){
						target=targets[i];
						distToTarg=distance[i];
					}
				}
				*/
				if(distToTarg>eHitRng){
					//print("Engaging the enemy");
					//Debug.Log (target);
					//Debug.Log (target.transform.position);
					agent.SetDestination(target.transform.position);
					//agent.ResetPath();
					//agent.SetDestination(Vector3.zero);
					anim.SetBool("Run",true);
				}
				else{
					//print ("Time to strike!");
					anim.SetBool("Run",false);
					agent.ResetPath();
					anim.SetTrigger("Strike");
					yield return new WaitForSeconds(0.3f);//примерное время до собственно удара в анимации
					MobController_2_1 targEnem=null;
					if(target!=null&&target.activeSelf==true&&((targEnem=target.GetComponent<MobController_2_1>())!=null)){
						targEnem.TakeDamage(hitDmg);
						targEnem.Pseudocollision((target.transform.position-transform.position).normalized*hitPwr);
					}
					yield return new WaitForSeconds(Mathf.Max(0,0.875f-cycleTime));//чуть меньше, чем актуальное время атаки, синхронизировать не удалось
				}
			}
			yield return new WaitForSeconds(cycleTime);
		}
	}
	void LifeEnd(){
		StopAllCoroutines ();
		CancelInvoke ();
		agent.enabled = false;
		isAlive = false;
		anim.enabled = false;
		metarig.isKinematic (false);
		ObjectPool.instance.rezEndEffect.Spawn (hips).transform.parent=null;
	}
}
