﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TutorialHand : MonoBehaviour {

	public Sprite handPoint, handPretap, handSlide;
	public Vector2 tapPivot, slidePivot;
	Image imgHand;
	RectTransform trans;
	List <GameObject> targets = new List<GameObject> ();
	public GameObject target,target2;
	Vector2 startPos,endPos;
	float quantTime = 1 / 30f;
	bool isActive;
	bool skipable;
	GameObject objHand;
	public Canvas canv;
	public RectTransform rezButton;
	public Transform rezThing;
	//public float scMod;

	// Use this for initialization
	void Awake(){
		trans = GetComponent<RectTransform> ();
		imgHand = GetComponent<Image> ();
		objHand = gameObject;
	}
	void Start () {
		imgHand.sprite = handPoint;
		//scMod = canv.scaleFactor;
	}
	
	// Update is called once per frame
	void Update () {

		if (isActive && skipable && interfaceControl.Instance.GS==interfaceControl.GameState.Tutorial && objHand.activeSelf == true ) {
			if(Input.GetMouseButtonDown(0)){
				Resume();
			}
		}
	}

	public void StartTutorial(int num){
		isActive = true;
		skipable = false;
		StartCoroutine ("Tutorial"+num.ToString());
	}

	IEnumerator Tutorial1(){

		targets.Clear();
		targets.AddRange(GameObject.FindGameObjectsWithTag("enemy"));
		for (int i=0; i<targets.Count; i++) {
			if(targets[i].activeSelf==false){
				targets.RemoveAt(i);
				i--;
			}
		}
		if (targets.Count > 0) {
			target = targets [0];
			startPos = trans.position;
			endPos = Camera.main.WorldToScreenPoint (target.GetComponent<MobController_2_1> ().hips.position);
			for (int i=1; i<=30; i++) {
				trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
				yield return StartCoroutine(TimelessWait (quantTime));
			}
			while (isActive) {
				yield return StartCoroutine(TimelessWait (0.5f));
				imgHand.sprite = handPretap;
				yield return StartCoroutine(TimelessWait (0.5f));
				imgHand.sprite = handPoint;
				skipable=true;
			}
		} else
			Resume();
	}
	IEnumerator Tutorial2(){
		target = GameObject.FindGameObjectWithTag ("toy");
		imgHand.sprite = handPretap;
		trans.pivot = slidePivot;
		startPos = trans.position;
		endPos = Camera.main.WorldToScreenPoint (target.transform.position);
		for (int i=1; i<=30; i++) {
			trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
			yield return StartCoroutine(TimelessWait (quantTime));
		}
		while (isActive) {
			imgHand.sprite=handSlide;
			yield return StartCoroutine(TimelessWait (0.3f));
			startPos=trans.position;
			endPos=startPos-Vector2.right*Screen.width/4f;
			for (int i=1; i<=30; i++) {
				trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
				yield return StartCoroutine(TimelessWait (quantTime));
			}
			skipable=true;
			imgHand.sprite=handPretap;
			yield return StartCoroutine(TimelessWait (0.4f));
			startPos=trans.position;
			endPos=Camera.main.WorldToScreenPoint (target.transform.position);
			for (int i=1; i<=30; i++) {
				trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
				yield return StartCoroutine(TimelessWait (quantTime));
			}
		}
	}
	IEnumerator Tutorial3(){
		target = GameObject.FindGameObjectWithTag ("toy");
		imgHand.sprite = handPretap;
		trans.pivot = slidePivot;
		startPos = trans.position;
		endPos = rezButton.position;
		for (int i=1; i<=30; i++) {
			trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
			yield return StartCoroutine(TimelessWait (quantTime));
		}
		imgHand.sprite=handSlide;
		//по хорошему бы анимацию реса, следующую за рукой, но время остановлено и она играть не будет...
		yield return StartCoroutine(TimelessWait (0.3f));
		startPos=trans.position;
		endPos=Camera.main.WorldToScreenPoint (target.transform.position);
		for (int i=1; i<=30; i++) {
			trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
			yield return StartCoroutine(TimelessWait (quantTime));
		}
		skipable = true;
		while (isActive) {
			imgHand.sprite=handPretap;
			yield return StartCoroutine(TimelessWait (0.6f));
			startPos=trans.position;
			endPos=rezButton.position;
			for (int i=1; i<=30; i++) {
				trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
				yield return StartCoroutine(TimelessWait (quantTime));
			}
			imgHand.sprite=handSlide;
			yield return StartCoroutine(TimelessWait (0.3f));
			startPos=trans.position;
			endPos=Camera.main.WorldToScreenPoint (target.transform.position);
			for (int i=1; i<=30; i++) {
				trans.position = Vector3.Lerp (startPos, endPos, i / 30f);
				yield return StartCoroutine(TimelessWait (quantTime));
			}
		}
	}


	IEnumerator TimelessWait(float waitingTime){
		float pauseEndTime = Time.unscaledTime + waitingTime;
		while (Time.unscaledTime < pauseEndTime) {
			yield return null;
		}
	}
	void Resume(){
		StopAllCoroutines();
		isActive=false;
		gameObject.SetActive(false);
		//Time.timeScale=1;
		interfaceControl.Instance.ResumeGame (); 
	}
}
