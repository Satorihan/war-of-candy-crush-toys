﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaisedSoldier : MonoBehaviour {
	Raiseable riser;
	MetaRigidbody metarig;
	Animator anim;
	bool isAlive;
	float cycleTime;
	public float hitDmg=2;
	public float hitRange=3;
	float eHitRng;
	public float hitPwr=30;
	//List <GameObject> targets = new List<GameObject> ();
	GameObject target;
	//List <float> distance = new List<float> ();
	float distToTarg;
	NavMeshAgent agent;
	public Transform hips;
	public GameObject shootPart;
	Vector3 tempv,tempv2;
	float animLength;
	Vector3 tempTargPos;
	float tempDist;
	// Use this for initialization
	void Awake(){
		riser = GetComponent<Raiseable> ();
		metarig = GetComponent<MetaRigidbody> ();
		anim = /*transform.GetChild(0).*/GetComponent<Animator> ();
		agent = GetComponent<NavMeshAgent> ();
		isAlive = false;
	}
	void Start () {
		anim.enabled = false;
		agent.enabled = false;
		//agent.Stop ();
		riser.MakeAlive = LifeStart;
		riser.UnmakeAlive = LifeEnd;
		riser.getupTime = 3;
		cycleTime = riser.cycleTime;
		shootPart.SetActive(false);
		eHitRng = hitRange * hitRange;
	}
	
	void LifeStart(){
		isAlive = true;
		metarig.isKinematic (true);
		anim.enabled = true;
		
		//transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y,0);
		//определить направление торса, тут нам поможет измерение поворота по Х. Х>0 - вниз, иначе вверх.
		transform.position = new Vector3 (hips.position.x, 0.734f, hips.position.z);
		if (transform.rotation.eulerAngles.x > 180) {
			anim.Play ("soldier_get_up1");
			//anim.SetTrigger("StandFromBack");//К сожалению - не работает: в самом начале анимации вставания перед корня в направлении ног, а не из торса.
			animLength=2.375f;
			transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y+transform.rotation.eulerAngles.z,0);
		} else {
			anim.Play ("soldier_get_up2");
			//anim.SetTrigger("StandFromFront");
			animLength=3.5f;
			transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y-transform.rotation.eulerAngles.z,0);
		}
		//transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y,0);
		Invoke ("ReadyForAction", animLength);
	}
	
	void ReadyForAction(){
		agent.enabled = true;
		//anim.applyRootMotion = false;
		StartCoroutine (SearchAndDestroy());
	}
	GameObject GetTarget(){
		target = null;
		//distToTarg = 0;
		for (int i=0; i<interfaceControl.Instance.enemies.Count; i++) {
			tempTargPos=interfaceControl.Instance.enemies[i].transform.position;
			if(tempTargPos.z<0&&tempTargPos.x>-27&&tempTargPos.x<13){
				tempDist=(tempTargPos-transform.position).sqrMagnitude;
				if(target==null||tempDist<distToTarg){
					target=interfaceControl.Instance.enemies[i];
					distToTarg=tempDist;
				}
			}
		}
		return target;
	}
	IEnumerator SearchAndDestroy(){
		while (isAlive) {
			/*
			targets.Clear();
			distance.Clear();
			targets.AddRange(GameObject.FindGameObjectsWithTag("enemy"));
			for (int i=0; i<targets.Count; i++) {
				if(targets[i].activeSelf==false){
					targets.RemoveAt(i);
					i--;
				}
			}
			if(targets.Count==0){//Врагов нет
				//print("Sector clear");
				agent.SetDestination(transform.position);
				anim.SetBool("Run",false);
			}
			*/
			GetTarget();
			if(target==null){
				//agent.SetDestination(transform.position);
				agent.ResetPath();
				anim.SetBool("Run",false);
			}
			else {//Враги таки есть
				//print("Enemy spotted");
				/*
				for(int i=0;i<targets.Count;i++){
					distance.Add((transform.position-targets[i].transform.position).magnitude);
				}
				target=targets[0];
				distToTarg=distance[0];
				for(int i=1;i<distance.Count;i++){
					if(distance[i]<distToTarg){
						target=targets[i];
						distToTarg=distance[i];
					}
				}
				*/
				if(distToTarg>eHitRng){
					//print("Engaging the enemy");
					//Debug.Log (target);
					//Debug.Log (target.transform.position);
					agent.SetDestination(target.transform.position);
					//agent.SetDestination(Vector3.zero);
					anim.SetBool("Run",true);
				}
				else{
					anim.SetBool("Run",false);
					agent.ResetPath();

					anim.SetTrigger("Shoot");
					shootPart.SetActive(true);
					Vector3 startQ=transform.forward;
					Vector3 endQ=new Vector3(target.transform.position.x-transform.position.x,0,target.transform.position.z-transform.position.z);
					for (int i=1; i<=10; i++) {
						transform.forward = Vector3.Lerp (startQ, endQ, i/10f);
						yield return new WaitForSeconds(0.2f/10);
					}
					//yield return new WaitForSeconds (0.17f);
					shootPart.SetActive(false);
					MobController_2_1 targEnem=null;
					if(target!=null&&target.activeSelf==true&&((targEnem=target.GetComponent<MobController_2_1>())!=null)){
						targEnem.TakeDamage(hitDmg);
						targEnem.Pseudocollision((target.transform.position-transform.position).normalized*hitPwr);
					}
					yield return new WaitForSeconds(Mathf.Max(0,0.638f-cycleTime));
				}
			}
			yield return new WaitForSeconds(cycleTime);
		}
	}
	void LifeEnd(){
		StopAllCoroutines ();
		CancelInvoke ();
		agent.enabled = false;
		isAlive = false;
		anim.enabled = false;
		metarig.isKinematic (false);
		shootPart.SetActive(false);
		ObjectPool.instance.rezEndEffect.Spawn (hips).transform.parent=null;
	}
}