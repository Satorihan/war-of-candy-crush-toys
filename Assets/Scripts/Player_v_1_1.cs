﻿using UnityEngine;
using System.Collections;

public class Player_v_1_1 : MonoBehaviour {
	public static Player_v_1_1 Instance;
	Ray ray;
	RaycastHit hit;
	//public Rigidbody rBody;
	public Rigidbody draggedItem;
	public Throwable itemToThrow;
	Vector3 vector;
	public float Speed;
	public float powerUp;
	public float power;

	public Vector2 startPos = Vector2.zero;
	public Vector2 endPos = Vector2.zero;
	public GameObject X;
	public Transform leftWall,rightWall,backWall;
	bool isDragging;
	Coroutine dragChecking;
	GameObject hitTarget;

	public enum StateT
	{
		none,
		candy,
		toy
	}
	public StateT[] touchState;
	bool touchActive;
	public GameObject[] touchObject;

	void Awake()
	{
		Instance = this;
	}
	void Start () {
		//State = StateT.none;
		X.SetActive (false);
		draggedItem = null;
		leftWall.GetComponent<Collider> ().enabled = false;
		rightWall.GetComponent<Collider> ().enabled = false;
		backWall.GetComponent<Collider> ().enabled = false;
		ray = Camera.main.ScreenPointToRay (Vector3.forward);
		if (Physics.Raycast (ray, out hit)) {
			leftWall.position = hit.point;
		} else
			print ("Left wall positioning failure");
		ray = Camera.main.ScreenPointToRay (new Vector3 (Screen.width,0,1));
		if (Physics.Raycast (ray, out hit)) { 
			rightWall.position = hit.point;
		} else
			print ("Right wall positioning failure");
		ray = Camera.main.ScreenPointToRay (new Vector3 (Screen.width/2f,-Screen.height/20f,1));
		if (Physics.Raycast (ray, out hit)) {
			backWall.position = hit.point;
		} else
			print ("Back wall positioning failure");
		leftWall.GetComponent<Collider> ().enabled = true;
		rightWall.GetComponent<Collider> ().enabled = true;
		backWall.GetComponent<Collider> ().enabled = true;
	}
	void TouchEmpty(int number){
		touchObject[number] = null;
		//touchState = (touchObject [1] || touchObject [2] || touchObject [3] || touchObject [4] || touchObject [5]);
	}
	void PickATarget(Vector3 point){
		ray = Camera.main.ScreenPointToRay (point);
		if (Physics.Raycast (ray, out hit)) {
			//Debug.Log (hit.collider.gameObject.name);
			if(MetaRigidbody.GetMetarig(hit.collider.transform)!=null)hitTarget=MetaRigidbody.GetMetarig(hit.collider.transform).gameObject;
			else hitTarget=hit.collider.gameObject;
			//print (hitTarget);
			if(hitTarget.name=="Clickable"){
				hitTarget=hitTarget.transform.parent.gameObject;
			}
			if(hitTarget.tag=="coin"){
				hitTarget.GetComponent<Coin>().Collect();
			}
			if(hitTarget.tag=="candy"){
				//print("candy identified");
				draggedItem=hitTarget.GetComponent<Rigidbody>();
				//dragChecking=StartCoroutine(DragCheck());
				isDragging = true;
				//draggedItem = draggedItem=itemToThrow.gameObject.GetComponent<Rigidbody>();
				draggedItem.isKinematic=true;
				draggedItem.gameObject.layer=13;
				X.SetActive(true);
			}
			else if(hitTarget.tag=="enemy"){
				//Transform temptrans=hitTarget.transform;
				MobController_2_1 temp=hitTarget.GetComponent<MobController_2_1>();
				//print(temp);
				if(temp!=null){
					temp.TakeDamage(2);
				}
			}
			else if(hitTarget.tag=="enemy2"){
				DatTrigger temp=hitTarget.GetComponent<DatTrigger>();
				if(temp!=null){
					//print ("HAND");
					temp.Clicked();
				}
			}
			else {
				itemToThrow=hitTarget.GetComponent<Throwable>();
				//print ("pish");
				if (itemToThrow!=null){
					startPos=Input.mousePosition;
					Invoke("MustThrow",0.3f);
					//print("ready to throw");
				}
			}
		}
	}
	void Update()
	{
		/*
		#if (UNITY_EDITOR || UNITY_STANDALONE)
		#endif
		#if UNITY_ANDROID
		#endif
		if(interfaceControl.Instance.GS==interfaceControl.GameState.Normal){
			if (Input.GetMouseButtonDown (0)) {
				if(Input.touchCount>0)
				{
					for(int j=0;j<Input.touchCount;j++)
					{
						PickATarget(Input.GetTouch(j).position);
					}
				}
				else PickATarget(Input.mousePosition);
			}
		}
		*/
		if (interfaceControl.Instance.GS == interfaceControl.GameState.Normal) {
			if (Input.GetMouseButtonDown (0)) {
				PickATarget (Input.mousePosition);
			}
		}
		if (Input.GetMouseButtonUp (0)) {
			endPos=Input.mousePosition;
			//print("unpish");
			if(dragChecking!=null){
				StopCoroutine(dragChecking);
				dragChecking=null;
			}
			if(!isDragging){
				/*
				if(itemToThrow!=null){
					ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					if (Physics.Raycast (ray, out hit)) {
						//print ("throw");
						itemToThrow.ThrowThis(hit.point.x-itemToThrow.transform.position.x,hit.point.z-itemToThrow.transform.position.z);
					}
				}
				*/
				MustThrow();
			}
			else {
				StopDrag();
			}
			itemToThrow=null;
		}

		if (isDragging && draggedItem!=null && Input.GetMouseButton (0)) {
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				vector=hit.point;
				vector.y=2f;
				draggedItem.transform.position=vector;
			}
		}


	}

	IEnumerator DragCheck(){//actually unused
		//print ("dragcheck");
		//yield return new WaitForSeconds (0.5f);
		//print ("dragcheck complete");
		if (Input.GetMouseButton (0)) {
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit)) {
				hitTarget=hit.collider.gameObject;
				if(hitTarget.name=="Clickable"){
					hitTarget=hitTarget.transform.parent.gameObject;
				}
				if (hitTarget==draggedItem.gameObject){
					isDragging = true;
					//draggedItem = draggedItem=itemToThrow.gameObject.GetComponent<Rigidbody>();
					draggedItem.isKinematic=true;
					draggedItem.gameObject.layer=13;
					X.SetActive(true);
				}
			}
		}
		yield return new WaitForEndOfFrame ();
	}


	public void StopDrag(){
		if (dragChecking != null) {
			StopCoroutine (dragChecking);
			dragChecking = null;
		} else {
			X.SetActive (false);
			if(draggedItem!=null){
				draggedItem.isKinematic = false;
				draggedItem.gameObject.layer = 12;
			}
			isDragging = false;
		}
		//isDragging=false;
		draggedItem = null;
	}

	void MustThrow(){
		if (itemToThrow != null) {
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				//print ("throw");
				hitTarget=hit.collider.gameObject;
				//print(hitTarget);
				if(hitTarget.name=="Clickable"){
					hitTarget=hitTarget.transform.parent.gameObject;
				}
				//print(hitTarget);
				//print(itemToThrow);
				if(hitTarget==itemToThrow.gameObject){
					itemToThrow=null;
					return;
				}
				itemToThrow.ThrowThis(hit.point.x-itemToThrow.transform.position.x,hit.point.z-itemToThrow.transform.position.z);
			}
			itemToThrow=null;
		}
	}

	/*
	void MustThrow(int number){
		if (touchObject[number] != null) {
			ray = Camera.main.ScreenPointToRay (Input.GetTouch(number).position);
			if (Physics.Raycast (ray, out hit)) {
				//print ("throw");
				hitTarget=hit.collider.gameObject;
				//print(hitTarget);
				if(hitTarget.name=="Clickable"){
					hitTarget=hitTarget.transform.parent.gameObject;
				}
				//print(hitTarget);
				//print(itemToThrow);
				if(hitTarget==touchObject[number]){
					TouchEmpty(number);
					return;
				}
				//itemToThrow.ThrowThis(hit.point.x-itemToThrow.transform.position.x,hit.point.z-itemToThrow.transform.position.z);
				touchObject[number].GetComponent<Throwable>().ThrowThis((hit.point.x-touchObject[number].transform.position.x), (hit.point.z-touchObject[number].transform.position.z));
			}
			itemToThrow=null;
		}
	}
	*/
}