﻿using UnityEngine;
using System.Collections;

public class LevelScripts : MonoBehaviour {
	//Actually, this should be used like a txt to contain level data in text form
	//One should use only static things there
	public static string[] GetLevelData(int lvl){
		return levelData [lvl].Split ('*');
	}


	/*
	level should be in format:
	Toy/Points
	...
	Toy/Points
	Time to wait
	Enemy/Points
	...
	Enemy/Points
	Time to wait
	Enemy/Points
	...
	Enemy/Points
	...
	Time to wait
	Enemy/Points
	...
	Enemy/Points

	Toys, Enemies and Points references are defined in MobSpawnControlScript
	*/

	public static string[] levelData =
	{
		//Level 1
		"1*" +
		"BE/B*" +
		"6*" +
		"BE/BC*" +
		"6*" +
		"BE/AC*" +
		"6*" +
		"DE/C*" +
		"6*" +
		"DE/A*" +
		"BE/B*" +
		"6*" +
		"BE/AC*" +
		"DE/B*" +
		"3*" +
		"DE/B*" +
		"6*" +
		"BE/ABC*" +
		"6*" +
		"DE/B*" +
		"BE/C*" +
		"6*" +
		"BE/AC*" +
		"BM/B*" +
		"6*" +
		"DE/AB*" +
		"6*" +
		"DE/CC*" +
		"BE/A*" +
		"BM/BB"
		,
		//Level 2
		"TBl/2*" +
		"TBx/1*" +
		"1*" +
		"DE/AC*" +
		"6*" +
		"BM/BBB*" +
		"6*" +
		"BM/ABC*" +
		"6*" +
		"BE/BBCC*" +
		"6*" +
		"DE/AA*" +
		"BM/BB*" +
		"6*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"4*" +
		"BM/C*" +
		"2*" +
		"BM/C*" +
		"2*" +
		"BM/C*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/A*" +
		"2*" +
		"BM/A*" +
		"2*" +
		"BM/A*" +
		"4*" +
		"DE/CC*" +
		"BS/C*" +
		"6*" +
		"DE/BB*" +
		"BS/B*" +
		"6*" +
		"DS/A"
		,
		//Level 3
		"TBl/2*" +
		"2*" +
		"BE/B*" +
		"2*" +
		"BE/AC*" +
		"2*" +
		"BE/BC*" +
		"2*" +
		"BE/AB*" +
		"3*" +
		"BE/ABC*" +
		"3*" +
		"BE/ABC*" +
		"3*" +
		"BM/AC*" +
		"4*" +
		"DE/AC*" +
		"5*" +
		"BS/AC*" +
		"6*" +
		"DS/B*" +
		"BE/B*" +
		"6*" +
		"BM/ABC*" +
		"5*" +
		"BM/ABC*" +
		"4*" +
		"BM/ABC*" +
		"3*" +
		"BM/ABC*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"3*" +
		"BS/ABC"
		,
		//Level 4
		"TC/1*" +
		"TS/2*" +
		"TBx/3*" +
		"2*" +
		"DE/B*" +
		"2*" +
		"DE/BB*" +
		"3*" +
		"DE/AA*" +
		"3*" +
		"DE/CCC*" +
		"4*" +
		"DS/B*" +
		"2*" +
		"DE/AA*" +
		"3*" +
		"DE/CCC*" +
		"4*" +
		"DE/AB*" +
		"3*" +
		"DE/AB*" +
		"DS/C*" +
		"4*" +
		"DE/B*" +
		"2*" +
		"DE/BA*" +
		"2*" +
		"DE/B*" +
		"2*" +
		"DS/C*" +
		"DE/B*" +
		"2*" +
		"DE/B*" +
		"2*" +
		"DE/BCC*" +
		"2*" +
		"DE/B*" +
		"2*" +
		"DS/BC*" +
		"4*" +
		"DE/BB*" +
		"3*" +
		"DE/CC*" +
		"2*" +
		"DE/AA*" +
		"1*" +
		"DE/B*" +
		"1*" +
		"DE/C*" +
		"1*" +
		"DE/A*" +
		"1*" +
		"DE/B*" +
		"1*" +
		"DE/C*" +
		"1*" +
		"DE/A*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"DE/BB*" +
		"2*" +
		"DE/CC"
		,
		//Level 5
		"TBl/1*" +
		"TBb/2*" +
		"TBr/3*" +
		"2*" +
		"BE/A*" +
		"2*" +
		"BE/BC*" +
		"2*" +
		"BM/AC*" +
		"2*" +
		"BM/AB*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BS/B*" +
		"2*" +
		"BM/BC*" +
		"3*" +
		"BS/C*" +
		"2*" +
		"BE/AB*" +
		"4*" +
		"BM/BC*" +
		"3*" +
		"BM/BC*" +
		"3*" +
		"BM/AB*" +
		"3*" +
		"BS/C*" +
		"3*" +
		"BE/ABC*" +
		"3*" +
		"BM/BC*" +
		"3*" +
		"BS/A*" +
		"BM/C*" +
		"3*" +
		"BS/B*" +
		"BM/A*" +
		"2*" +
		"BS/C*" +
		"BM/B*" +
		//MADNESS?
		//THOSE ARE BARBIE!!!
		"4*" +
		"BE/AB*" +
		"2*" +
		"BE/BC*" +
		"2*" +
		"BE/AB*" +
		"2*" +
		"BE/AC*" +
		"2*" +
		"BS/B*" +
		"2*" +
		"BM/BC*" +
		"2*" +
		"BS/C*" +
		"2*" +
		"BE/AC*" +
		"2*" +
		"BE/AB*" +
		"2*" +
		"BE/BC*" +
		"2*" +
		"BS/A*" +
		"3*" +
		"BS/C*" +
		"3*" +
		"BS/B*" +
		"3*" +
		"BM/AB"
		,
		//Level 6
		"TBl/12*" +
		"TC/3*" +
		"3*" +
		"BE/AB*" +
		"DE/C*" +
		"4*" +
		"BE/BB*" +
		"DE/C*" +
		"4*" +
		"DE/CC*" +
		"4*" +
		"BE/AB*" +
		"DE/C*" +
		"4*" +
		"BM/AB*" +
		"DE/C*" +
		"4*" +
		"BE/AB*" +
		"DS/C*" +
		"3*" +
		"DE/C*" +
		"2*" +
		"BE/AB*" +
		"2*" +
		"DE/C*" +
		"3*" +
		"BM/AB*" +
		"DE/C*" +
		"4*" +
		"BM/AB*" +
		"DE/CC*" +
		"4*" +
		"BE/A*" +
		"BS/B*" +
		"DE/C*" +
		"DS/C*" +
		"5*" +
		"BS/AB*" +
		"Bx/C*" +
		"4*" +
		"BE/AB*" +
		"3*" +
		"BE/AB*" +
		"DE/C*" +
		"3*" +
		"BM/B*" +
		"DE/C*" +
		"2*" +
		"BS/A*" +
		"4*" +
		"BE/ABB*" +
		"3*" +
		"BM/AB*" +
		"3*" +
		"BM/AB*" +
		"DE/CC*" +
		"3*" +
		"BS/A*" +
		"BM/B*" +
		"DS/C*" +
		"DE/C*" +
		"5*" +
		"BM/AB*" +
		"DE/CC*" +
		"4*" +
		"BM/AB*" +
		"DE/C*" +
		"4*" +
		"BM/A*" +
		"4*" +
		"BM/B*" +
		"DE/C"
		,
		//Level 7
		"TBb/1*" +
		"TC/2*" +
		"TBx/3*" +
		"Bx/D*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BM/BC*" +
		"DE/A*" +
		"2*" +
		"DE/B*" +
		"BM/AC*" +
		"2*" +
		"BM/AB*" +
		"DE/C*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"DE/AB*" +
		"BM/C*" +
		"2*" +
		"DE/BC*" +
		"BM/A*" +
		"2*" +
		"DE/CA*" +
		"BM/B*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"DE/A*" +
		"BM/B*" +
		"BS/C*" +
		"2*" +
		"DE/B*" +
		"BM/C*" +
		"BS/A*" +
		"2*" +
		"DE/C*" +
		"BM/A*" +
		"BS/B*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"DE/CCC*" +
		"2*" +
		"DE/BBB*" +
		"2*" +
		"DS/A*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BS/ABC*" +
		"2*" +
		"BM/CCC*" +
		"2*" +
		"BM/BBB*" +
		"2*" +
		"BS/A*" +
		"BM/BC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"DS/A*" +
		"BS/BC*" +
		"2*" +
		"DS/B*" +
		"BS/AC*" +
		"2*" +
		"DS/C*" +
		"BS/AB*" +
		"2*" +
		"DS/ABC"
		,
		//Level 8
		"TS/1*" +
		"TBl/2*" +
		"TBr/3*" +
		"Bx/C*" +//BX
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"BS/ABC*" +
		"2*" +
		"DE/BBBB*" +
		"2*" +
		"DE/CCCC*" +
		"2*" +
		"DS/A*" +
		"2*" +
		"DE/CCCC*" +
		"2*" +
		"DE/BBBB*" +
		"2*" +
		"Bx/D*" +//BX
		"2*" +
		"DS/A*" +
		"2*" +
		"BM/BB*" +
		"DE/CC*" +
		"2*" +
		"DE/BB*" +
		"BM/CC*" +
		"2*" +
		"BS/A*" +
		"DE/BC*" +
		"BM/BC*" +
		"2*" +
		"BS/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"DE/ABBCC*" +
		"2*" +
		"BM/ABBCC*" +
		"2*" +
		"BS/A*" +
		"BE/BC*" +
		"2*" +
		"Bx/D*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"BS/A*" +
		"BM/BBCC*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"BE/BC*" +
		"BM/ABC*" +
		"2*" +
		"DS/A*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"BS/ABC*" +
		"2*" +
		"DS/AC*" +
		"3*" +
		"BE/AB*" +
		"3*" +
		"BE/BC*" +
		"3*" +
		"BE/AC"
		,
		//Level 9
		"TC/1*" +
		"TBx/2*" +
		"TBb/3*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/AB*" +
		"2*" +
		"BE/BC*" +
		"2*" +
		"BE/AC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BM/AA*" +
		"1*" +
		"BM/BB*" +
		"1*" +
		"BM/CC*" +
		"2*" +
		"DE/CC*" +
		"1*" +
		"DE/BB*" +
		"1*" +
		"DE/AA*" +
		"1*" +
		"BS/C*" +
		"1*" +
		"BS/B*" +
		"1*" +
		"BS/A*" +
		"2*" +
		"BE/AABBCC*" +
		"3*" +
		"BM/ABC*" +
		"2*" +
		"BM/C*" +
		"1*" +
		"BM/B*" +
		"1*" +
		"BM/A*" +
		"1*" +
		"DS/C*" +
		"1*" +
		"DS/B*" +
		"1*" +
		"DS/A*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"DE/CCCCC*" +
		"2*" +
		"BM/ABB*" +
		"2*" +
		"DS/C*" +
		"3*" +
		"BM/ABBCC*" +
		"3*" +
		"BS/A*" +
		"1*" +
		"BS/B*" +
		"1*" +
		"BS/C*" +
		"1*" +
		"BS/A*" +
		"1*" +
		"BS/B*" +
		"1*" +
		"BS/C*" +
		"1*" +
		"BS/A*" +
		"1*" +
		"BS/B*" +
		"1*" +
		"BS/C*" +
		"1*" +
		"BM/AB*" +
		"2*" +
		"BM/BC*" +
		"2*" +
		"BM/AC*" +
		"2*" +
		"BS/ABC*" +
		"3*" +
		"DE/BBBB*" +
		"4*" +
		"DE/CCCCC*" +
		"3*" +
		"DS/A*" +
		"3*" +
		"DS/BC*" +
		"3*" +
		"BM/ABC*" +
		"2*" +
		"BE/ABC"
		,
		//Level 10
		"TS/1*" +
		"TBr/2*" +
		"TBb/3*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"1*" +
		"Bx/D*" +
		"1*" +
		"BS/A*" +
		"1*" +
		"BS/B*" +
		"1*" +
		"BS/C*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"BS/A*" +
		"1*" +
		"BS/B*" +
		"1*" +
		"BS/C*" +
		"2*" +
		"DE/AB*" +
		"1*" +
		"DS/C*" +
		"2*" +
		"DE/AC*" +
		"1*" +
		"DS/B*" +
		"2*" +
		"DE/BC*" +
		"1*" +
		"DS/A*" +
		"1*" +
		"Bx/D*" +
		"1*" +
		"BS/A*" +
		"2*" +
		"BM/A*" +
		"BS/B*" +
		"2*" +
		"BE/A*" +
		"BM/B*" +
		"BS/C*" +
		"2*" +
		"BE/B*" +
		"BM/C*" +
		"2*" +
		"BE/C*" +
		"2*" +
		"DE/ABCC*" +
		"2*" +
		"DE/ABBC*" +
		"2*" +
		"DE/BC*" +
		"DS/A*" +
		"2*" +
		"DE/BBCC*" +
		"2*" +
		"DS/A*" +
		"1*" +
		"Bx/D*" +
		"1*" +
		"BM/ABC*" +
		"2*" +
		"BS/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"DE/ABC*" +
		"2*" +
		"DS/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/A*" +
		"1*" +
		"BE/B*" +
		"1*" +
		"BE/C"





		/*
		//LEVEL 1
		"1*" +
		"BE/B*" +
		"6*" +
		"BE/BC*" +
		"6*" +
		"BE/AC*" +
		"6*" +
		"DE/C*" +
		"6*" +
		"DE/A*" +
		"BE/B*" +
		"6*" +
		"BE/AC*" +
		"DE/B"
		,
		//LEVEL 2
		"1*" +
		"DE/B*" +
		"6*" +
		"BE/ABC*" +
		"6*" +
		"DE/B*" +
		"BE/C*" +
		"6*" +
		"BE/AC*" +
		"BM/B*" +
		"6*" +
		"DE/AB*" +
		"6*" +
		"DE/CC*" +
		"BE/A*" +
		"BM/BB"
		,
		//LEVEL 3
		"TBl/2*" +
		"TBx/1*" +
		"1*" +
		"DE/AC*" +
		"6*" +
		"BM/BBB*" +
		"6*" +
		"BM/ABC*" +
		"6*" +
		"BE/BBCC*" +
		"6*" +
		"DE/AA*" +
		"BM/BB*" +
		"6*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC"
		,
		//LEVEL 4
		"TBl/1*" +
		"TC/3*" +
		"2*" +
		"BE/ABC*" +
		"4*" +
		"BM/C*" +
		"2*" +
		"BM/C*" +
		"2*" +
		"BM/C*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/A*" +
		"2*" +
		"BM/A*" +
		"2*" +
		"BM/A*" +
		"4*" +
		"DE/CC*" +
		"BS/C*" +
		"6*" +
		"DE/BB*" +
		"BS/B*" +
		"6*" +
		"DS/A"
		,
		//LEVEL 5
		"TBl/2*" +
		"2*" +
		"BE/B*" +
		"2*" +
		"BE/AC*" +
		"2*" +
		"BE/BC*" +
		"2*" +
		"BE/AB*" +
		"3*" +
		"BE/ABC*" +
		"3*" +
		"BE/ABC*" +
		"3*" +
		"BM/AC*" +
		"4*" +
		"DE/AC*" +
		"5*" +
		"BS/AC*" +
		"6*" +
		"DS/B*" +
		"BE/B*" +
		"6*" +
		"BM/ABC"
		,
		//LEVEL 6
		"TS/1*" +
		"TBb/3*" +
		"2*" +
		"BE/ABC*" +
		"4*" +
		"BM/ABC*" +
		"4*" +
		"DE/ABC*" +
		"4*" +
		"BS/ABC*" +
		"4*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"2*" +
		"BE/ABC*" +
		"4*" +
		"BE/AA*" +
		"DS/B*" +
		"DE/CC*" +
		"6*" +
		"DE/CCCCCC*" +
		"8*" +
		"BE/BBB*" +
		"DE/CCCCC*" +
		"8*" +
		"DS/AB*" +
		"DE/CCCCC"
		,
		//LEVEL 7
		"TBr/1*" +
		"TC/2*" +
		"2*" +
		"BS/BBC*" +
		"6*" +
		"BS/ACC*" +
		"6*" +
		"BM/ABC*" +
		"6*" +
		"BM/ABBBC*" +
		"6*" +
		"BM/B*" +
		"2*" +
		"BM/B*" +
		"BE/A*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/B*" +
		"BE/A*" +
		"BS/C*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BS/AB*" +
		"2*" +
		"BM/B*" +
		"2*" +
		"BM/ABC*" +
		"2*" +
		"BM/B*" +
		"BE/AC*" +
		"2*" +
		"BM/BBB*" +
		"BS/CC*" +
		"BE/CCCC"
		,
		//LEVEL 8
		"TBl/2*" +
		"TS/3*" +
		"Bx/D*" +
		"6*" +
		"BE/ABC*" +
		"6*" +
		"BM/ABC*" +
		"6*" +
		"BS/ABC*" +
		"6*" +
		"DE/ABC*" +
		"6*" +
		"DS/ABC*" +
		"8*" +
		"BE/C*" +
		"BM/C*" +
		"BS/C*" +
		"6*" +
		"BE/B*" +
		"BM/B*" +
		"BS/B*" +
		"4*" +
		"DS/A*" +
		"DE/CC*" +
		"3*" +
		"DS/B*" +
		"DE/CC*" +
		"3*" +
		"DS/C*" +
		"DE/CC"
		,
		//LEVEL 9
		"TC/2*" +
		"TBl/1*" +
		"2*" +
		"BE/AACC*" +
		"BM/BB*" +
		"5*" +
		"DE/CCC*" +
		"4*" +
		"DE/BBB*" +
		"4*" +
		"DE/CCC*" +
		"4*" +
		"DE/AAA*" +
		"4*" +
		"BS/ABC*" +
		"4*" +
		"BS/ABC*" +
		"4*" +
		"DS/ABC"
		,
		//LEVEL 10 - INTERESTING
		"TBb/1*" +
		"TS/2*" +
		"TBr/3*" +
		"2*" +
		"BE/ABC*" +
		"3*" +
		"BE/ABC*" +
		"3*" +
		"BE/ABC*" +
		"3*" +
		"Bx/D*" + 
		"2*" +
		"BM/ABC*" +
		"3*" +
		"BM/ABC*" +
		"3*" +
		"BM/ABC*" +
		"4*" +
		"BS/ABC*" +
		"4*" +
		"BS/ABC*" +
		"4*" +
		"DE/ABC*" +
		"3*" +
		"DE/ABC*" +
		"3*" +
		"DE/ABC*" +
		"2*" +
		"Bx/C*" +
		"4*" +
		"DE/ABC*" +
		"5*" +
		"DS/ABC*" +
		"5*" +
		"DS/ABC*" +
		"5*" +
		"DS/ABC*" +
		"2*" +
		"BE/ABC"
		*/
	};
}
