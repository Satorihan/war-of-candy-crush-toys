﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {
	public GameObject playButton;
	public GameObject exitButton;
	public GameObject backButton;
	//public GameObject settingsButton;
	//public GameObject SettingsPanel;
	public GameObject logo;
	public GameObject soundButton;
	public GameObject musicButton;
	public GameObject levelSelectButton;
	public GameObject levelSelectPanel;
	// Use this for initialization
	void Start () {
		ShowMain ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void HideInterface(){
		playButton.SetActive (false);
		exitButton.SetActive (false);
		backButton.SetActive (false);
		logo.SetActive (false);
		soundButton.SetActive (false);
		musicButton.SetActive (false);
		levelSelectButton.SetActive (false);
		levelSelectPanel.SetActive (false);
	}
	public void ShowMain(){
		HideInterface ();
		playButton.SetActive (true);
		exitButton.SetActive (true);
		logo.SetActive (true);
		soundButton.SetActive (true);
		musicButton.SetActive (true);
		levelSelectButton.SetActive (true);
	}
	public void ShowLevelSelect(){
		HideInterface ();
		backButton.SetActive (true);
		levelSelectPanel.SetActive (true);
	}
	public void Play(){
		//Application.LoadLevel ("game");
		LoadingComponent.Instance.LoadNextLevel("game");
	}
	public void Exit(){
		Application.Quit ();
	}
}
