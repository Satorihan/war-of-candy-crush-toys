﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SpawnControl : MonoBehaviour {
	public static SpawnControl instance;
//	public GameObject candy;
//	public GameObject Mob;
//	public int sizeCandy;
//	public Transform[] pointCandy;
//	public Transform[] pointEnemy;
//	public float timeMobSpawn;
	int i;
//	int tm;
	public List<GameObject> MobScene = new List<GameObject>();


	[System.Serializable]
	public class Mobs
	{
		public int size;
		public GameObject ObjectMob;
		public List<Transform> pointEnemy = new List<Transform>();
	}
	
	[System.Serializable]
	public class Candys
	{
		public int size;
		public GameObject ObjectCandy;
		public Transform[] pointCandy;
	}
	
	[System.Serializable]
	public class Coints
	{
		public GameObject ObjectCoint;
	}
	[System.Serializable]
	public class Particl
	{
		public GameObject ParticleSystem;
	}
	public Particl[] Part;
	public Coints Coint;
	public Candys Candy;
	public Mobs[] Mob;

	void Awake()
	{
		instance = this;
	}
	void Start () 
	{
		InvokeRepeating("SpawnCandy", 0, 0.5F);
		InvokeRepeating("SpawnMob", 2, 2F);
//		var pools = instance.Candy;
//		pools[i].ObjectCandy.
	}

	void Update () 
	{

	}
	void SpawnMob()
	{
//		var pools = instance.Mob;

		MobScene.Clear();

/*		if(tm < Mob[0].size){
			Mob[Random.Range(0,2)].ObjectMob.Spawn(Mob[0].pointEnemy[Random.Range(0,Mob[0].pointEnemy.Count)].transform.position,Mob[0].pointEnemy[Random.Range(0,Mob[0].pointEnemy.Count)].transform.rotation);
		}
*/
//		MobScene.AddRange(GameObject.FindGameObjectsWithTag("enemy"));
//		tm = MobScene.Count;
//        MobScene[MobScene.Count-1].GetComponent<MobController_2_0>().enabled = true;
//		MobScene[MobScene.Count-1].GetComponent<MobController_2_0>().Chase();
	}

	void SpawnCandy()
	{
		var pools = instance.Candy;
		i++;
		if(i <= pools.size)
			pools.ObjectCandy.Spawn(pools.pointCandy[Random.Range(0,0)].transform.position,pools.ObjectCandy.transform.rotation = new Quaternion(Random.Range(0,180),Random.Range(0,180),Random.Range(0,180),0));
		else
			CancelInvoke("SpawnCandy");
	}
	public void SpawnCoint(Transform t)
	{
		Coint.ObjectCoint.Spawn(new Vector3(t.transform.position.x,1.5f,t.transform.position.z),Coint.ObjectCoint.transform.rotation);
	}
	public void SpawnPartical(Transform t, int i)
	{
		Part[i].ParticleSystem.Spawn(t.transform.position,Part[i].ParticleSystem.transform.rotation);
	}
}
