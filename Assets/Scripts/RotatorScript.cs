using UnityEngine;
using System.Collections;

/// <summary>
/// Вращение вокруг центра объекта или центра модели
/// </summary>
public class RotatorScript : MonoBehaviour {
	public bool BoundsCenterMode;
	public float Speed = 1;
	public Vector3 RotateVector = new Vector3(0, 1, 0);

	
	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
		if(BoundsCenterMode)
			transform.RotateAround(GetComponent<Renderer>().bounds.center, RotateVector, Speed * Time.deltaTime);
		else
			transform.RotateAround(transform.position, RotateVector, Speed * Time.deltaTime);

	}
}
