﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour {

	public int health;
	public float moveSpeed;
	GameObject target;
	GameObject home;
    NavMeshAgent nAgent;
	bool iCathCandy;


	public List<GameObject> targets = new List<GameObject>();       //Каждый экземпляр класса енеми хранит в своем листе данные обо всех конфетках
	public List<float> DistancesForEachCandys = new List<float>();  // И дистанции до них

	public float minDistance;
	public int indexOfMinDistanceGameObject;
	       					  

	void CathCandy()//Предположительно обработка конфетных взаимодействий
	{
		if (Vector3.Distance (transform.position, target.transform.position) <= 1f) {
			iCathCandy = true;

			target.transform.parent = this.gameObject.transform;
			target.tag = "Untagged";
			target.GetComponent<Rigidbody> ().useGravity = false;
			target.GetComponent<Collider> ().enabled = false;

			if (Vector3.Distance (transform.position, GameObject.Find ("spawnPoint1").gameObject.transform.position) <= 1f && iCathCandy)
			{
				Destroy(this.gameObject);
			}

		} else 
		{
			iCathCandy = false;
		}

	}


	/*Следующая функция обновляет список конфет и выбирает ближайшую конфетку
													(Но мы помним что енеми несколько и все это происходит на каждом из них, 
													следовательно целесообразно установить интервал времени 
													через который функция выбора цели будет выполняться, а не делать это в апдейт, Сделаем через invokeRepeating в старте с рейтом в 0.1 c)*/



	void ChooseTarget() 
	{
		DistancesForEachCandys.Clear();
		targets.Clear ();
		targets.AddRange(GameObject.FindGameObjectsWithTag("candy")); 

        for (int i = 0; i<targets.Count; i++)
		{
			float dist = (Vector3.Distance(targets[i].transform.position,transform.position));  //получаем дистанцию добавляем ее в массив дистанций
			DistancesForEachCandys.Add(dist);
		}


		
		for(int i = 0; i < DistancesForEachCandys.Count; i ++) 
		{
             if(i == 0) 
			{
			minDistance = DistancesForEachCandys[i];
			indexOfMinDistanceGameObject = i;
			}
			else
			if(DistancesForEachCandys[i] < minDistance ) 
			{
				minDistance  = DistancesForEachCandys[i];
				indexOfMinDistanceGameObject = i;
				target = targets[indexOfMinDistanceGameObject];//странно, что нет аналогичной обработки для i==0
			}
		}


	}


	void Start () 
	{
    nAgent = GetComponent<NavMeshAgent> ();
		home = GameObject.Find ("spawnPoint1");
	InvokeRepeating("ChooseTarget", 0, 0.2F); 
}

	void Update () {
		CathCandy ();

        nAgent.speed = moveSpeed;
		if (target != null)
		if (!iCathCandy) {
			nAgent.SetDestination (target.transform.position);
		} else 
		{
			nAgent.SetDestination (home.transform.position);
		}
	}




}
