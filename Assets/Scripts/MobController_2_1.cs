﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class MobController_2_1 : MonoBehaviour {
	//public static MobController_2_1 Instance;
	//public List<GameObject> targets = new List<GameObject>();
	public List<float> distance = new List<float>();
	public Candy target;
	public Candy candy;
	public float DistanceTarget;
	public float Speed;
	public float CycleTime;
	public float health=1;
	public float maxHealth = 1;
	public float catchPicktime=0.5f;
	public float catchTime=1;
	protected float catchDeltaTime;
	public Animator anim;
	protected NavMeshAgent agent;
//	bool isVulnerable=true;
	public float resistance=25;
	public float stunTime=5;
	protected Rigidbody rgdbody;
	protected MetaRigidbody metarig;
	protected bool hasMetarig;
	protected Rigidbody candyRigidbody;
	//Vector3 candEndP;
	//Quaternion candEndQ;
	public Transform hips;
	public Transform hand;
	public float handLength=1;
	public float standFrontTime=1;
	public float standBackTime=1;
	protected float tempMagn;
	protected YieldInstruction waitPlz;
	public enum State
	{
		none,
		chase,
		escape,
		death,
		stay
	}
	
	
	protected State st;
	public State state
	{
		get { return st; }
		set
		{
			//State prevState = st;
			st = value;
			switch (st)
			{
			case State.chase:
				StartCoroutine(GoingForCandy());
				break;
				
			case State.escape:
				StopAllCoroutines();
				Escape();
				break;
				
			case State.death:
				StopAllCoroutines();
				Death();
				break;
				
			case State.stay:
				Stay();
				break;

			}
		}
	}
	public State StateEnemy
	{
		get { return state; }
	}

	public string mobName;


	protected void OnDisable()
	{
		if (agent)
			agent.enabled = false;
	}
	public virtual void OnEnable()
	{
		//Speed = MobSpawnControlScript.instance.thingRef [name].speed;
		//maxHealth = MobSpawnControlScript.instance.thingRef [name].hp;
		if (!agent)
			agent = GetComponent<NavMeshAgent> ();
		//else
			agent.enabled = true;
		//state = State.chase;
		agent.speed = Speed;
		agent.avoidancePriority = 50;
		//agent.enabled = true;
		/*
		if (metarig == null) {
			metarig = GetComponent<MetaRigidbody> ();
			if (metarig != null) {
				hasMetarig = true;
				//metarig.ModifyRigidbody((Rigidbody rig)=>{rig.isKinematic=true;});
			}
		}
		*/
		//metarig.Reinit (); Не помню, зачем это было нужно
		Chase ();
		health = maxHealth;
	}

	protected virtual void Awake()
	{
		//Instance = this;
		waitPlz = new WaitForSeconds (CycleTime);
		//print (waitPlz);
		rgdbody = GetComponent<Rigidbody> ();
		metarig = GetComponent<MetaRigidbody> ();
		anim = GetComponent<Animator> ();
		if (metarig != null) {
			hasMetarig = true;
			//metarig.ModifyRigidbody((Rigidbody rig)=>{rig.isKinematic=true;});
			metarig.metacollisionEnterAction=MetaCollisionEnter;//(Collision col)=>{print(health);});//Нихрена себе, оно работает
		}
	}

	protected void Start () {
		agent = GetComponent<NavMeshAgent>();
		//targets = interfaceControl.Instance.candies;
		//state = State.chase;
		agent.speed = Speed;
		//agent.enabled = true;
		Chase ();
		catchDeltaTime = (catchTime - catchPicktime) / 10f;

	}
	
	
	protected void FixedUpdate ()
	{
		if(state == State.escape&&candy!=null&&interfaceControl.Instance.GS==interfaceControl.GameState.Won){
			DropCandy();
		}
	}
	public virtual void TakeDamage(float damage=1){
		health -= damage;
		if (health <= 0)
			state = State.death;
		else {
			ObjectPool.instance.hitEffect.Spawn (hips).transform.parent = null;
			ObjectPool.instance.DamageSound.Spawn (hips).transform.parent = null;
		}
	}

	protected virtual IEnumerator GoingForCandy()
	{
		while(state==State.chase)
		{
			//targets = interfaceControl.Instance.candies;
			//targets.Clear();
			distance.Clear();
			//targets.AddRange(GameObject.FindGameObjectsWithTag("candy")); 
			// target = null;
			
			if(interfaceControl.Instance.candies.Count == 0)
				state = State.escape;
			else{
				for (int i = 0; i<interfaceControl.Instance.candies.Count; i++)
				{
					float dist = (transform.position - interfaceControl.Instance.candies[i].transform.position).sqrMagnitude;
					distance.Add(dist);
				}
				for (int i = 0; i<distance.Count; i++)
				{
					if(i == 0){
						DistanceTarget = distance[i];
						target = interfaceControl.Instance.candies[0];
					}else{
						if(distance[i] < DistanceTarget){
							DistanceTarget = distance[i];
							target = interfaceControl.Instance.candies[i];
						}
					}
				}
				//Debug.Log (agent.transform.name);
				//Debug.Log (agent.enabled);
				//Debug.Log(agent);
				//print (agent.transform.position);
				agent.SetDestination(target.transform.position);
				//try{ agent.SetDestination(target.position);}
				//catch{Death();}
				//Debug.Log(target.position);
				//anim.Play("Move");
				
				if(interfaceControl.Instance.candies.Count == 1)
				{
					target = interfaceControl.Instance.candies[0];
				}
				if(DistanceTarget < 9f){
					GetTheCandy(target);
				}
				if(candy!=null){//по хорошему попадать сюда не должно, но на всякий случай проверка пусть будет.
					GetTheCandy(candy);
				}
				if(Application.loadedLevelName == "game"){
					if(interfaceControl.Instance && interfaceControl.Instance.EndGame)
						state=State.escape;
				}

			}
			yield return waitPlz;
			//Debug.Log ("__");

		}
	}
	/*
	protected void GetTheCandySimplified(Transform targCandy){
		candy = targCandy;
		if (Player_v_1_1.Instance.draggedItem!=null&&candy==Player_v_1_1.Instance.draggedItem.transform){
			Player_v_1_1.Instance.StopDrag();
		}
		candy.tag = "Untagged";
		candy.gameObject.layer = 13;
		//StopCoroutine("GoingForCandy");
		state = State.escape;
		target = null;
		candyRigidbody=candy.GetComponent<Rigidbody> ();
		//agent.SetDestination(GameObject.FindWithTag("Exit").transform.position);
		candyRigidbody.useGravity = false;
		candyRigidbody.isKinematic = true;
		candy.transform.parent = this.gameObject.transform;
		candy.transform.localPosition = new Vector3(0,agent.height,1f/transform.localScale.z+agent.radius*0.8f);
		candy.transform.localRotation = new Quaternion(0,0,0,0);
	}
	*/
	protected void GetTheCandy(Candy targCandy){
		agent.ResetPath ();
		//if (targCandy.tag != "candy")return;//Это неправильная конфета, не буду её брать
		candy = targCandy;
		if (Player_v_1_1.Instance.draggedItem!=null&&candy==Player_v_1_1.Instance.draggedItem.transform){
			Player_v_1_1.Instance.StopDrag();
		}
		//StopCoroutine("GoingForCandy");
		//state = State.escape;
		/*
		candy.tag = "Untagged";
		candy.gameObject.layer = 13;
		candyRigidbody=candy.GetComponent<Rigidbody> ();
		candyRigidbody.useGravity = false;
		candyRigidbody.isKinematic = true;
		candy.GetComponent<BoxCollider> ().enabled = false;
		*/
		//print (candy);
		candy.CaptureCandy();
		candy.transform.parent = gameObject.transform;
		target = null;
		StopAllCoroutines ();
		StartCoroutine (PickingUp());
		//candy.transform.localPosition = new Vector3(0,agent.height,1f/transform.localScale.z+agent.radius*0.8f);
		//candy.transform.localRotation = new Quaternion(0,0,0,0);
	}
	protected IEnumerator PickingUp(){
		anim.Play ("Catch",-1,0f);
		yield return new WaitForSeconds (catchPicktime);
		Vector3 candStartP = candy.transform.localPosition;
		Quaternion candStartQ = candy.transform.localRotation;
		Vector3 candEndP = new Vector3(0,hand.position.y/transform.localScale.y,handLength);
		//кручу-верчу, запутать хочу
		Quaternion candEndQ = Quaternion.Euler(90+180*Mathf.Round((candy.transform.localRotation.eulerAngles.x-90)/180f),candy.transform.localRotation.eulerAngles.y,180*Mathf.Round(candy.transform.localRotation.eulerAngles.z/180f));

		//candy.transform.localRotation = new Quaternion(0,0,0,0);
		for (int i=1; i<=10; i++) {
			candy.transform.localPosition = Vector3.Lerp(candStartP,candEndP,i/10f);
			candy.transform.localRotation = Quaternion.Slerp(candStartQ,candEndQ,i/10f);
			yield return new WaitForSeconds(catchDeltaTime);
		}
		interfaceControl.Instance.candies.Remove (candy);
		agent.avoidancePriority--;
		state = State.escape;
	}
	protected void Escape()
	{
		//print ("escape");
		//anim.Play ("Catch");
		GameObject[] exits;
		GameObject targetExit;
		float exitDist,tempDist;
		exits = GameObject.FindGameObjectsWithTag ("Exit");
		if (exits.Length < 1)
			Death ();//ВЫХОДА НЕТ
		else {
			targetExit=exits[0];
			exitDist=(transform.position-targetExit.transform.position).sqrMagnitude;
			for (int i=1;i<exits.Length;i++){
				tempDist=(transform.position-exits[i].transform.position).sqrMagnitude;
				if(tempDist<exitDist){
					targetExit=exits[i];
					exitDist=tempDist;
				}
			}
			if(exitDist<3){
				EscapeComplite();
				return;
			}
			agent.SetDestination(targetExit.transform.position);
		}
		anim.Play("Run");
	}
	protected void Death()
	{
		//Debug.Log(state);
		DropCandy ();
		interfaceControl.Instance.enemies.Remove (gameObject);
		gameObject.Recycle();
		
		//SpawnControl.instance.SpawnCoint(gameObject.transform);
		ObjectPool.instance.coin.Spawn (hips).transform.parent=null;
		//SpawnControl.instance.SpawnPartical(gameObject.transform,0);
		ObjectPool.instance.deathEffect.Spawn (hips).transform.parent=null;
		ObjectPool.instance.DeathSound.Spawn (hips).transform.parent=null;
		interfaceControl.Instance.GetEnergy ();

	}
	protected virtual void DropCandy(){
		if (!gameObject.activeInHierarchy)
			return;
		if (candy != null) {
			//interfaceControl.Instance.candies.Add (candy.gameObject);
			interfaceControl.Instance.ReturnCandy(candy);
			/*
			candy.tag = "candy";
			candy.gameObject.layer = 12;
			candyRigidbody.useGravity = true;
			candyRigidbody.isKinematic = false;
			candy.GetComponent<BoxCollider> ().enabled = true;
			*/
			candy.ReleaseCandy();
			candy.transform.parent = null;
			candy = null;
		}
	}
	public void EscapeComplite()
	{
		//print ("escape complete");
		if (state != State.escape)
			return;
		if(candy != null){
			if(interfaceControl.Instance.GS==interfaceControl.GameState.Normal||interfaceControl.Instance.GS==interfaceControl.GameState.Tutorial){
				/*
				candy.tag = "candy";
				candy.gameObject.layer = 12;
				candy.GetComponent<BoxCollider> ().enabled = true;
				candyRigidbody.useGravity = true;
				candyRigidbody.isKinematic = false;
				*/
				candy.ReleaseCandy();
				candy.transform.parent = null;
				candy.Recycle();
				candy = null;
				interfaceControl.Instance.enemies.Remove (gameObject);
				gameObject.Recycle();
				interfaceControl.Instance.CandyLost ();
			}
			else{
				DropCandy();
			}
		}else{
			interfaceControl.Instance.enemies.Remove (gameObject);
			gameObject.Recycle();
		}
	}
	protected void Stay()
	{
		StopAllCoroutines();
		DropCandy ();
		agent.enabled = false;
		if(gameObject.activeInHierarchy) StartCoroutine (PrepareToStandUp());//Проверка - чтоб при убийстве другомобом не было приколов
	}
	public void OnStay()
	{
		state = State.stay;
	}
	public void Chase()
	{
		metarig.isKinematic (true);
		agent.enabled = true;
		state = State.chase;
		agent.speed = Speed;
		anim.enabled = true;
		anim.Play ("Move");
	}
	protected IEnumerator PrepareToStandUp(){
		yield return new WaitForSeconds (stunTime);
		if (hasMetarig)
			//metarig.isKinematic (true);
			metarig.ModifyRigidbody ((Rigidbody rig)=>{rig.isKinematic=true;/*rig.gameObject.layer=15;*/});
		else
			rgdbody.isKinematic = true;
		anim.enabled = true;
		transform.position = new Vector3 (hips.position.x, 0.754f, hips.position.z);
		//print (transform.rotation.eulerAngles);
		if (transform.rotation.eulerAngles.x < 180) {
			anim.Play("UpT");
			transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y-transform.rotation.eulerAngles.z,0);
			yield return new WaitForSeconds(standFrontTime);
			//print("Front");
		} else {
			anim.Play("UpB");
			transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y+transform.rotation.eulerAngles.z,0);
			yield return new WaitForSeconds(standBackTime);
			//print("back");
		}

		Chase ();
	}
	protected void MetaCollisionEnter(Collision col){
		if (!gameObject.activeInHierarchy||col.gameObject.layer==2||col.gameObject.layer==8||col.gameObject.layer==18)//чтоб не прокало на выключенных и не запинаться об пол и невидимые стены
			return;
		tempMagn = col.relativeVelocity.magnitude;
		if (tempMagn > resistance) {
			//print(col.collider.gameObject);
			MetaRigidbody attachedMetarig = MetaRigidbody.GetMetarig (col.collider.transform);
			//print(attachedMetarig);
			GameObject colObj;
			if (attachedMetarig != null) {
				colObj=attachedMetarig.gameObject;
			}
			else{
				colObj=col.collider.gameObject;
			}
			//print(colObj);
			if(colObj==gameObject){//Да, при особом невезении моб ронял себя сам
				//print("YU DO THIS TO ME?!");
				return;
			}
			//здесь обработка столкновения. При низкой скорости - ничего, при достаточной - рэгдолл и всё, приехали.
			if (colObj.tag == "toy" || colObj.tag == "enemy") {
				Stay ();
				//print("gg");
				Vector3 temp = col.relativeVelocity;
				bool isResisting=anim.enabled;
				anim.enabled = false;
				//print("hitten");
				if (hasMetarig) {
					metarig.ModifyRigidbody ((Rigidbody rig) => {
						rig.isKinematic = false;
						if(isResisting)rig.velocity = temp * ((tempMagn - resistance) / tempMagn);
						//rig.gameObject.layer=18;
					});
				} else {
					rgdbody.isKinematic = false;
					if(isResisting)rgdbody.velocity = temp * ((tempMagn - resistance) / tempMagn);
				}
			}
		}
	}
	public void Pseudocollision(Vector3 vel){
		if (!gameObject.activeInHierarchy)
			return;
		tempMagn = vel.magnitude;
		if (tempMagn < resistance)
			return;
		else {
			vel*=((tempMagn-resistance)/tempMagn);
			Stay();
			anim.enabled=false;
			//print ("pseudocollision");
			if (hasMetarig) {
				metarig.ModifyRigidbody ((Rigidbody rig) => {
					rig.isKinematic = false;
					rig.velocity = vel;
					//rig.gameObject.layer=18;
				});
			} else {
				rgdbody.isKinematic = false;
				rgdbody.velocity = vel;
			}
		}
	}
}