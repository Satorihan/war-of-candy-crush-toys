using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class LiveToyBall : MonoBehaviour 
{
	float soundSpawnDelay=0.2f;

	//public Transform target;
	//public float strength=30;
	public float jumpCap=12;
	Rigidbody rig;
	Vector3 vertComp;
	Vector3 horDir;
	float targDist;
	public bool isActive=true;
	//List <GameObject> targets = new List<GameObject> ();
	public GameObject target;
	//List <float> distance = new List<float> ();
	float distToTarg;
	bool isEngaging=false;
	bool isDangerous=true;
	Raiseable riser;
	public List<Material> skins;
	public SkinnedMeshRenderer face;
	bool maySound=true;
	Vector3 tempTargPos;
	float tempDist;
	float eHitRng;

	//public float attDist=7;

	void Awake(){
		rig = GetComponent<Rigidbody> ();
		vertComp=new Vector3(0,30,0);
		isEngaging = false;
		isDangerous = true;
		riser = GetComponent<Raiseable> ();
		riser.MakeAlive = Activation;
		riser.UnmakeAlive = Deactivation;
	}
	void Start () 
	{
		//transform.DOJump (target.transform.position , 50, 1, 0.5f).SetEase (Ease.Linear);
		int temp = Random.Range (0, skins.Count);
		//print ("Reskin to: " + temp.ToString ());
		Material[] tempMat=new Material[face.materials.Length];
		for (int i=0; i<face.materials.Length; i++) {
			//print ("Reskining material "+i.ToString());
			//face.sharedMaterials[i]=skins[temp];
			tempMat[i]=skins[temp];
		}
		face.sharedMaterials = tempMat;
		eHitRng = jumpCap * jumpCap;
	}

	IEnumerator Chase(){//получилось почти неплохо, но всё-таки коряво
		while (isActive) {
			horDir = target.transform.position - transform.position;
			targDist = horDir.magnitude;
			horDir = horDir.normalized * Mathf.Min (targDist, jumpCap);
			rig.velocity = horDir + vertComp;
			yield return new WaitForSeconds (1);
		}
	}
	/*GameObject GetTarget(){//old
		targets.Clear();
		distance.Clear();
		targets.AddRange(GameObject.FindGameObjectsWithTag("enemy"));
		for (int i=0; i<targets.Count; i++) {
			if(targets[i].activeSelf==false){
				targets.RemoveAt(i);
				i--;
			}
		}
		if (targets.Count == 0) {//Врагов нет
			target = null;
			return null;
		} else {//Враги таки есть
			//print("Enemy spotted");
			for (int i=0; i<targets.Count; i++) {
				distance.Add ((transform.position - targets [i].transform.position).magnitude);
			}
			target = targets [0];
			distToTarg = distance [0];
			for (int i=1; i<distance.Count; i++) {
				if (distance [i] < distToTarg) {
					target = targets [i];
					distToTarg = distance [i];
				}
			}
			return target;
		}
	}
	*/
	GameObject GetTarget(){
		target = null;
		//distToTarg = 0;
		for (int i=0; i<interfaceControl.Instance.enemies.Count; i++) {
			tempTargPos=interfaceControl.Instance.enemies[i].transform.position;
			if(tempTargPos.z<0&&tempTargPos.x>-27&&tempTargPos.x<13){
				tempDist=(tempTargPos-transform.position).sqrMagnitude;
				if(target==null||tempDist<distToTarg){
					target=interfaceControl.Instance.enemies[i];
					distToTarg=tempDist;
				}
			}
		}
		return target;
	}
	void OnCollisionEnter(Collision col){
		if (isActive) {
			if (isEngaging) {
				rig.useGravity = true;
				isEngaging = false;
			}
			GameObject hitTarget;
			if (MetaRigidbody.GetMetarig (col.collider.transform) != null)
				hitTarget = MetaRigidbody.GetMetarig (col.collider.transform).gameObject;
			else
				hitTarget = col.collider.gameObject;
			GetTarget ();
			//if (target != null)target = target.GetComponent<MobController_2_1> ().hips.gameObject;
			if (hitTarget.tag != "enemy") {
				if (target == null) {
					rig.velocity = vertComp;
				} else if (distToTarg > eHitRng) {
					horDir = target.transform.position - transform.position;
					horDir.y=0;
					//targDist = horDir.magnitude;
					//horDir = horDir.normalized * Mathf.Min (targDist, jumpCap);
					horDir = horDir.normalized * jumpCap * 2;
					rig.velocity = horDir + vertComp;
				} else {
					horDir = target.transform.position - transform.position;
					//targDist = horDir.magnitude;
					horDir = horDir.normalized * distToTarg;
					rig.velocity = horDir + vertComp;
					Invoke ("EngageTarget", 0.5f);
				}
			} else {
				//rig.velocity *= 2;
				if(isDangerous){
					hitTarget.GetComponent<MobController_2_1>().TakeDamage(2);
					isDangerous=false;
					Invoke("GetDangerous",0.5f);//It's time to get dangerous!
				}
			}
		}
		//ObjectPool.instance.BallSound.Spawn ().transform.parent=null;
		if (maySound) {
			ObjectPool.instance.BallSound.Spawn ().transform.parent=null;
			maySound=false;
			Invoke("ReactivateSound",soundSpawnDelay);
		}
	}
	void ReactivateSound(){
		maySound = true;
	}
	void GetDangerous(){
		isDangerous = true;
	}
	void EngageTarget(){

		if (!target)
			return;
		isEngaging = true;
		rig.useGravity = false;
		rig.velocity = (target.GetComponent<MobController_2_1> ().hips.position - (transform.position+transform.up)).normalized*jumpCap*3f;
	}
	public void Activation(){
		rig.velocity = vertComp;
		isActive = true;
	}
	public void Deactivation(){
		isActive = false;
		CancelInvoke ();
		isDangerous = true;
		rig.useGravity = true;
		ObjectPool.instance.rezEndEffect.Spawn (transform).transform.parent=null;
	}
}
