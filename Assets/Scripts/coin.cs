﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {
	Rigidbody rb;
	public Vector3 vector;
	public int LifeTime;
//	public int tTime;
//	int i = 0;
//	Renderer rend;
	void Awake()
	{
		//rb = GetComponent<Rigidbody>();
//		rend = GetComponent<Renderer>();
	}


	void OnEnable()
	{
		//rb.AddForce(vector);
		Invoke("DestroyCoin",LifeTime);
//		InvokeRepeating("TTime",0,0.25f);
	}
	void Start () 
	{

	}

	void Update () 
	{

	}
	//void OnMouseDown()
	public void Collect()
	{
		gameObject.Recycle();
//		i = 0;
//		rend.enabled = true;
		CancelInvoke("DestroyCoin");
//		CancelInvoke("TTime");
//		SpawnControl.instance.SpawnPartical(gameObject.transform,1);
		ObjectPool.instance.coinEffect.Spawn (transform).transform.parent = null;
//		if(Application.loadedLevelName == "game")
		interfaceControl.Instance.Coin(1);
	}
	void DestroyCoin()
	{
//		i++;
//		if(i > LifeTime){
			gameObject.Recycle();
//			i=0;
//			rend.enabled = true;
//			i = 0;
			CancelInvoke("DestroyCoin");
//			CancelInvoke("TTime");
//		}
	}

//	void TTime()
//	{
//		if(i > tTime){
//			if(rend.enabled == true)
//				rend.enabled = false;
//			else
//				rend.enabled = true;
//		}
//	}
}
