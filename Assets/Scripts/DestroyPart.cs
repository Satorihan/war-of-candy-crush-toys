﻿using UnityEngine;
using System.Collections;

public class DestroyPart : MonoBehaviour {
	//float timer = 0;
	// Use this for initialization
	void OnEnable () 
	{
		Invoke("Dest",1f);
	}
	
	// Update is called once per frame
	/*
	void Update () 
	{
		if(timer < 1)
		{
			timer+= Time.deltaTime;
		}else{
			gameObject.Recycle();
			timer = 0;
		}
	
	}
	*/
	void Dest()
	{
		gameObject.Recycle();
	}
}
