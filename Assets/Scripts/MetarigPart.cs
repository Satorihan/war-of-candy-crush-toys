﻿using UnityEngine;
using System.Collections;
using System;

public class MetarigPart : MonoBehaviour {
	Action<Collision> MetacollisionEnter;
	// Use this for initialization
	void Start () {
		MetacollisionEnter = MetaRigidbody.GetMetarig (transform).OnMetacollisionEnter;
	}
	void OnCollisionEnter(Collision col){
		if (MetacollisionEnter != null)
			MetacollisionEnter (col);
		//else print ("Could not connect metarigpart "+name);
	}
	// Update is called once per frame
	/* //Использовать местный апдейт - ОЧЕНЬ плохая идея.
	void Update () {
	
	}
	*/
}
