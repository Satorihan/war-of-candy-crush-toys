﻿using UnityEngine;
using System.Collections;

public class EnemyShieldedScript : MobController_2_1 {
	bool isInvulnerable=false;
	public GameObject invulEffect;
	public override void OnEnable(){
		base.OnEnable ();
		isInvulnerable = false;
		invulEffect.SetActive (false);
	}
	public override void TakeDamage(float damage=1){
		if (isInvulnerable) {
			return;
		}
		health -= damage;
		if (health <= 0)
			state = State.death;
		else {
			isInvulnerable=true;
			invulEffect.SetActive (true);
			Invoke("GetVulnerable",1.5f);
			ObjectPool.instance.hitEffect.Spawn (hips).transform.parent = null;
			ObjectPool.instance.DamageSound.Spawn (hips).transform.parent = null;
		}
	}
	void GetVulnerable(){
		isInvulnerable = false;
		invulEffect.SetActive (false);
	}
}
