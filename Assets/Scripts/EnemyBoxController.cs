﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBoxController : MonoBehaviour {//мечтает ли коробка о навмешагенте?..
	//ONE SHALL NOT SPAWN IT BEHIND A DOOR
	float movementDelta=5;//сдвиг при движении
	public bool isActive;
	public List<GameObject> targets = new List<GameObject>();
	public List<float> distance = new List<float>();
	public Transform target;
	public float DistanceTarget;
	public Transform Candy;
	public Collider Hand;
	Rigidbody candyRigidbody;
	Animator anim;
	//public float CycleTime=0.5f;
	public float minCycle, maxCycle;
	float deltaCycle;
	NavMeshAgent agent;
	public float hp;
	float maxHp;

	// Use this for initialization
	void Awake(){
		anim = GetComponent<Animator> ();
		agent = GetComponent<NavMeshAgent> ();
		minCycle = Mathf.Max (minCycle, 0);
		maxCycle = Mathf.Max (maxCycle, 0);
		if (maxCycle < minCycle) {
			deltaCycle = minCycle;
			minCycle = maxCycle;
			maxCycle = deltaCycle;
		}
		deltaCycle = maxCycle - minCycle;
		maxHp = hp;
	}
	void Start () {

	}
	void OnEnable(){
		StartCoroutine (WantCandy());
		hp = maxHp;
		Hand.enabled = false;
	}
	// Update is called once per frame
	void Update () {
	
	}
	IEnumerator WantCandy(){
		//print("Now I live");

		while (isActive) {
			yield return new WaitForSeconds(minCycle+deltaCycle*Random.value);//прототипный вариант, без дополнительного выжидания при близких кликах
			//targets.Clear();
			distance.Clear();
			//targets.AddRange(GameObject.FindGameObjectsWithTag("candy")); 
			//targets=interfaceControl.Instance.candies;
			// target = null;
			
			if(interfaceControl.Instance.candies.Count == 0){
				//state = State.escape;//нет конфет - выжидает
			}
			else{
				for (int i = 0; i<interfaceControl.Instance.candies.Count; i++)
				{
					float dist = (transform.position - interfaceControl.Instance.candies[i].transform.position).magnitude;
					distance.Add(dist);
					
					
				}
				for (int i = 0; i<distance.Count; i++)
				{
					if(i == 0){
						DistanceTarget = distance[i];
						target = interfaceControl.Instance.candies[0].gameObject.GetComponent<Transform>();
					}else{
						if(distance[i] < DistanceTarget){
							DistanceTarget = distance[i];
							target = interfaceControl.Instance.candies[i].gameObject.GetComponent<Transform>();
							//agent.SetDestination(target.position);
						}
						//						else{
						//							target = targets[0].gameObject.GetComponent<Transform>();
						//							agent.SetDestination(target.position);
						//						}
					}
					//if(DistanceTarget == distance[0])target = targets[0].gameObject.GetComponent<Transform>();
				}
				//Время невероятного перемещения к цели!
				//agent.SetDestination(target.position);

				//Debug.Log(target.position);
				//anim.Play("Move");
				if(DistanceTarget < movementDelta){
					GetTheCandy(target);
				}
				else
					yield return StartCoroutine(Move());
				if(Candy!=null){//по хорошему попадать сюда не должно, но на всякий случай проверка пусть будет.
					GetTheCandy(Candy);
				}
				//if(CGame.Instance.State == CGame.CGameState.Win)isActive=false;
			}
		}
	}
	IEnumerator Move(){
		//print ("now I move");
		agent.speed = 0.2f;
		agent.angularSpeed = 720;
		agent.SetDestination(target.position);
		yield return new WaitForSeconds(1);
		agent.angularSpeed = 0;
		agent.speed = 0;
		anim.Play ("Move");
		Hand.enabled = true;
		yield return new WaitForSeconds (0.73f);
		Vector3 startPos = transform.position;
		Vector3 endPos = transform.position + movementDelta * transform.forward;
		for (int i=1; i<=15; i++) {
			transform.position = Vector3.Lerp (startPos, endPos, i/15f);
			yield return new WaitForSeconds (0.27f/15f);
		}
		Hand.enabled = false;
	}
	void GetTheCandy(Transform targCandy){
		Candy = targCandy;
		Candy.tag = "Untagged";
		Candy.gameObject.layer = 13;
		target = null;
		candyRigidbody=Candy.GetComponent<Rigidbody> ();
		//agent.SetDestination(GameObject.FindWithTag("Exit").transform.position);//коробка не отступает
		//candyRigidbody.useGravity = false;
		candyRigidbody.isKinematic = true;
		Candy.GetComponent<BoxCollider> ().enabled=false;
		StartCoroutine (VacuumCandy());
	}
	IEnumerator VacuumCandy(){
		Vector3 startQ=transform.forward;
		Vector3 endQ=new Vector3(Candy.position.x-transform.position.x,0,Candy.position.z-transform.position.z);
		for (int i=1; i<=15; i++) {
			transform.forward = Vector3.Lerp (startQ, endQ, i/15f);
			yield return new WaitForEndOfFrame();
		}
		Vector3 startPos = Candy.transform.position;
		Vector3 endPos = transform.position;
		anim.Play ("Catch");
		yield return new WaitForSeconds (0.73f);
		if ((Candy.transform.position - transform.position).magnitude > movementDelta)CandyLost ();
		for (int i=1; i<=15; i++) {
			Candy.transform.position = Vector3.Lerp (startPos, endPos, i/15f);
			yield return new WaitForSeconds (0.27f/15f);
		}
		interfaceControl.Instance.candies.Remove (Candy.GetComponent<Candy>());
		Candy.Recycle ();
		interfaceControl.Instance.CandyLost ();
		Candy = null;
		StopAllCoroutines ();
		isActive = false;
	}
	void DropCandy(){
		Candy.GetComponent<BoxCollider> ().enabled = true;
		candyRigidbody.isKinematic = false;
		Candy.gameObject.layer = 12;
		Candy = null;
		candyRigidbody = null;
	}
	void CandyLost(){
		StopAllCoroutines ();
		DropCandy ();
		StartCoroutine (WantCandy());
	}
	public void HandClick(){
		//print ("tick");
		TakeDamage (2);
	}
	public void TakeDamage(float dmg){
		//print ("tock");
		hp -= dmg;
		if (hp <= 0) {
			Death();
		}
	}
	void Death(){
		if (Candy!=null) DropCandy();
		StopAllCoroutines ();
		SpawnControl.instance.SpawnCoint(gameObject.transform);
		SpawnControl.instance.SpawnPartical(gameObject.transform,0);
		gameObject.Recycle ();
		interfaceControl.Instance.GetEnergy ();
	}
}
