﻿using UnityEngine;
using System.Collections;
//using DG.Tweening;

public class Candy : MonoBehaviour {
	Rigidbody rgdbody;
	BoxCollider cllider;

	void Awake(){
		rgdbody = GetComponent<Rigidbody> ();
		cllider = GetComponent<BoxCollider> ();
	}
	void OnEnable(){
		ReleaseCandy ();//Предполагается, что конфеты не начинают игру в руках у мобья.
	}
	/*
	void Start () 
	{
		transform.DOShakeScale(0.5f,0.5f,10,90);
	}
	*/
	/*
	public static void GetThat(GameObject target){
		target.GetComponent<Candy> ().CaptureCandy ();
	}
	public static void GetThat(Transform target){
		GetThat (target.gameObject);
	}
	public static void ReleaseThat(GameObject target){
		target.GetComponent<Candy> ().ReleaseCandy ();
	}
	public static void ReleaseThat(Transform target){
		ReleaseThat (target.gameObject);
	}
	*/
	public void CaptureCandy(){
		tag = "Untagged";
		gameObject.layer = 13;
		rgdbody.useGravity = false;
		rgdbody.isKinematic = true;
		cllider.enabled = false;
	}
	public void ReleaseCandy(){
		tag = "candy";
		gameObject.layer = 12;
		rgdbody.useGravity = true;
		rgdbody.isKinematic = false;
		cllider.enabled = true;
	}
}
