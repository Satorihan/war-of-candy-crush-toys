﻿using UnityEngine;
using System.Collections;

public class Throwable : MonoBehaviour {
	public float extraPower;
	bool isHavingMetarig;
	Rigidbody rig;
	MetaRigidbody metarig;
	public Collider clickCatcher;
	// Use this for initialization
	void Start(){
		metarig = GetComponent<MetaRigidbody> ();
		if (metarig != null) {
			isHavingMetarig = true;
		} else {
			rig = GetComponent<Rigidbody> ();
		}
		//conForce = GetComponent<ConstantForce> ();
	}
	void OnEnable(){
		//health = maxHealth;
		if (clickCatcher)
			clickCatcher.enabled = true;
	}
	void OnDisable(){
		if (clickCatcher)
			clickCatcher.enabled = false;
	}
	/*
	void OnCollisionEnter(Collision col)
	{

	}
	*/
	public void ThrowThis(Vector2 dir){
		if (!this.enabled) {
			//print ("You shall not pass!");
			return;
		}
		//ThrowThis (dir.x, dir.y);
		transform.position += Vector3.up;
		dir += dir.normalized * extraPower;
		Vector3 temp = new Vector3 (dir.x,0,dir.y);
		//RecursiveThrow (dir, transform);
		if (isHavingMetarig && metarig.enabled)
			//metarig.SetVelocity (temp);
			metarig.ModifyRigidbody ((Rigidbody rigg)=>{rigg.velocity=temp;});
		else
			//RecursiveThrow (dir, transform);
			rig.velocity = temp; //Vector3.right * dir.x + Vector3.forward * dir.y;
		//rig.AddForce (Vector3.right * dir.x + Vector3.forward * dir.y);
	}
	/*
	void RecursiveThrow(Vector2 dir,Transform currentTrans){
		Rigidbody temp = currentTrans.GetComponent<Rigidbody>();
		if (temp != null) {
			temp.velocity = Vector3.right * dir.x + Vector3.forward * dir.y;//лучше переписать на более оптимальное
		}
		if (currentTrans.childCount > 0) {
			for(int i=0;i<currentTrans.childCount;i++){
				RecursiveThrow(dir,currentTrans.GetChild(i));
			}
		}

	}
	*/
	public void ThrowThis(float x, float y){
		ThrowThis (new Vector2(x,y));
		//transform.position += Vector3.up*0.1f;
		//conForce.enabled = false;
		//rig.velocity = (Vector3.right * x + Vector3.forward * y + Vector3.up * Mathf.Sqrt (Mathf.Abs(x) + Mathf.Abs(y)));
		//rig.AddForce (Vector3.up * (rig.mass * 1));
	}
}
