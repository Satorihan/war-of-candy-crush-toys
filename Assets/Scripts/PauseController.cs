﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseController : MonoBehaviour {
	public static PauseController Instance;
	public Image soundButton;
	public Image musicButton;
	public static bool isSoundEnabled,isMusicEnabled;
	public Sprite soundEnabled, soundDisabled, musicEnabled, musicDisabled;
	AudioSource bgMusic;

	// Use this for initialization
	void Awake(){
		Instance = this;
		isSoundEnabled = (PlayerPrefs.GetInt ("sound", 1) != 0 ? true : false);
		isMusicEnabled = (PlayerPrefs.GetInt ("music", 1) != 0 ? true : false);
		if (isMusicEnabled) {
			musicButton.sprite = musicEnabled;
		} else {
			musicButton.sprite = musicDisabled;
		}
		if (isSoundEnabled) {
			soundButton.sprite = soundEnabled;
		} else {
			soundButton.sprite = soundDisabled;
		}
		bgMusic = Camera.main.GetComponent<AudioSource> ();
		if (bgMusic != null)
			bgMusic.volume = (isMusicEnabled ? 1 : 0);
	}

	void Start () {
		isSoundEnabled = (PlayerPrefs.GetInt ("sound", 1) != 0 ? true : false);
		isMusicEnabled = (PlayerPrefs.GetInt ("music", 1) != 0 ? true : false);
		if (isMusicEnabled) {
			musicButton.sprite = musicEnabled;
		} else {
			musicButton.sprite = musicDisabled;
		}
		if (isSoundEnabled) {
			soundButton.sprite = soundEnabled;
		} else {
			soundButton.sprite = soundDisabled;
		}
		bgMusic = Camera.main.GetComponent<AudioSource> ();
		if (bgMusic != null)
			bgMusic.volume = (isMusicEnabled ? 1 : 0);
	}

	public void OnSoundButton_Click(){
		if(isSoundEnabled){
			SetSoundEnabled(false);
			soundButton.sprite = soundDisabled;
		}else{
			SetSoundEnabled(true);
			soundButton.sprite = soundEnabled;
		}
	}
	public void OnMusicButton_Click(){
		if(isMusicEnabled){
			SetMusicEnabled(false);
			musicButton.sprite = musicDisabled;
		}else{
			SetMusicEnabled(true);
			musicButton.sprite = musicEnabled;
		}
		if (bgMusic != null)
			bgMusic.volume = (isMusicEnabled ? 1 : 0);
	}
	public static void SetSoundEnabled(bool value)
	{
		isSoundEnabled = value;
		PlayerPrefs.SetInt("sound", (!value?0:1));
	}
	
	public static void SetMusicEnabled(bool value)
	{
		isMusicEnabled = value;
		PlayerPrefs.SetInt("music", (!value?0:1));
	}
}
