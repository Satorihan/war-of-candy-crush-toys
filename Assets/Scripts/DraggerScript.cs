﻿using UnityEngine;
using System.Collections;

public class DraggerScript : MobController_2_1 {
	public float dragDistSqr;
	public float dragSpeed = 3;
	public Transform markCandy;
	public ParticleSystemRenderer rayOfTargeting;
	YieldInstruction waitCatchPlz,waitCatchPlz1,waitCatchPlz2,waitQuant;
	int dragIterations;
	Vector3 dragDir;
	protected override void Awake(){
		base.Awake ();
		waitCatchPlz = new WaitForSeconds (catchTime);
		waitCatchPlz1 = new WaitForSeconds (catchPicktime);
		waitCatchPlz2 = new WaitForSeconds (catchTime-catchPicktime);
		dragIterations = Mathf.CeilToInt(catchTime * 30);
		waitQuant = new WaitForSeconds (1/30f);
		//rayOfTargeting.
	}
	public override void OnEnable ()
	{
		base.OnEnable ();
		markCandy.gameObject.SetActive (false);
		rayOfTargeting.gameObject.SetActive (false);
	}
	protected override IEnumerator GoingForCandy()
	{
		while(state==State.chase)
		{
			distance.Clear();
			if(interfaceControl.Instance.candies.Count == 0)
				state = State.escape;
			else{
				for (int i = 0; i<interfaceControl.Instance.candies.Count; i++)
				{
					float dist = (transform.position - interfaceControl.Instance.candies[i].transform.position).sqrMagnitude;
					distance.Add(dist);
				}
				for (int i = 0; i<distance.Count; i++)
				{
					if(i == 0){
						DistanceTarget = distance[i];
						target = interfaceControl.Instance.candies[0];
					}else{
						if(distance[i] < DistanceTarget){
							DistanceTarget = distance[i];
							target = interfaceControl.Instance.candies[i];
						}
					}
				}
				agent.SetDestination(target.transform.position);
				if(DistanceTarget < 9f){
					//candy.tag="candy";
					GetTheCandy(target);
				}
				else if(/*DistanceTarget < dragDistSqr && */transform.position.x>-25 && transform.position.x<11 && transform.position.z<1){
					StartDrag();
				}
				if(Application.loadedLevelName == "game"){
					if(interfaceControl.Instance && interfaceControl.Instance.EndGame)
						state=State.escape;
				}

			}
			yield return waitPlz;
		}
	}
	void StartDrag()
	{
		StopAllCoroutines ();
		candy = target;
		//candyRigidbody=target.GetComponent<Rigidbody>();
		//candy.tag = "Untagged";
		//candy.gameObject.layer = 13;
		//candyRigidbody=candy.GetComponent<Rigidbody>();
		candy.CaptureCandy ();
		interfaceControl.Instance.candies.Remove (candy);
		//agent.ResetPath ();
		agent.enabled = false;
		markCandy.parent = candy.transform;
		markCandy.localPosition = Vector3.zero;
		markCandy.gameObject.SetActive (true);
		rayOfTargeting.gameObject.SetActive (true);
		dragDir = (transform.position - candy.transform.position).normalized*dragSpeed/30;
		StartCoroutine (DragCandy());
		//start channel animation

	}
	IEnumerator DragCandy()
	{
		while (candy) {
			DistanceTarget=(transform.position-candy.transform.position).magnitude;
			rayOfTargeting.lengthScale=DistanceTarget*2+2;//Какой-то неясной длинны получается
			if(DistanceTarget<3f)
			{
				agent.enabled=true;
				GetTheCandy(candy);
				break;
			}
			else{
				//if(Player_v_1_1.Instance.draggedItem&&target==Player_v_1_1.Instance.draggedItem.transform)Player_v_1_1.Instance.StopDrag();
				anim.SetTrigger("catch");
				//yield return waitCatchPlz1;
				//надо как-то двигать конфету
				//candyRigidbody.velocity=(transform.position-candy.transform.position).normalized*dragSpeed;
				//yield return waitCatchPlz2;
				for(int i=0;i<dragIterations;i++){
					candy.transform.position+=dragDir;
					yield return waitQuant;
				}

			}
		}
		markCandy.parent = transform;
		markCandy.localPosition = Vector3.zero;
		markCandy.gameObject.SetActive (false);
		rayOfTargeting.gameObject.SetActive (false);
		yield return waitPlz;
		agent.enabled = true;
		if (candy == null)
			Chase ();
		else
			Escape ();
	}
	protected override void DropCandy(){
		base.DropCandy ();
		markCandy.gameObject.SetActive (false);
		markCandy.parent = transform;
		//markCandy.localPosition = Vector3.zero;
	}
}
