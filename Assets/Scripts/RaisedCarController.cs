﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaisedCarController : MonoBehaviour {
	Rigidbody rig;
//	BoxCollider carColl;
	public bool isActive=false;
	bool Deploing;
//	List <GameObject> targets = new List<GameObject> ();
	public GameObject target;
	Transform targHips;
//	GameObject lastTarget;
	Vector3 toTarget;
//	List <float> distance = new List<float> ();
//	float distToTarg;
//	public float forwLCap=4;
	public float forwCap=15;
	public float acc=7;
	public float rotCap=120;
	float speed;
//	float eFCap,eRCap,eAcc;
	Raiseable riser;
	float cycleTime;
	public float smallCycleTime;//=0.1f;
	float quantTime = 1 / 30f;
	Vector3 startPos;
	Vector3 endPos;
	Quaternion startQ;
	Quaternion endQ;
	float startF;
	float endF;
	float ticks;
	Vector3 tempTargPos;
	float tempDist;
	// Use this for initialization
	void Awake(){
		riser=GetComponent<Raiseable> ();
		rig = GetComponent<Rigidbody> ();
//		carColl = GetComponent<BoxCollider> ();
	}
	void Start () {
		riser.MakeAlive = Activate;
		riser.UnmakeAlive = Deactivate;
		cycleTime = riser.cycleTime;
//		eFCap = forwCap * cycleTime;
//		eRCap = rotCap * cycleTime;
		//print (rig.constraints);
		if (cycleTime < quantTime)
			cycleTime = quantTime;
		if (smallCycleTime < quantTime)
			smallCycleTime = quantTime;
		ticks = Mathf.Ceil(cycleTime / smallCycleTime);
//		lastTarget = null;
//		eAcc = acc * smallCycleTime;

	}
	/*
	GameObject GetTarget(){
		targets.Clear();
		distance.Clear();
		targets.AddRange(GameObject.FindGameObjectsWithTag("enemy"));
		for (int i=0; i<targets.Count; i++) {
			if(targets[i].activeSelf==false){
				targets.RemoveAt(i);
				i--;
			}
		}
		if (targets.Count == 0) {//Врагов нет
			target = null;
			return null;
		} else if (targets.Count == 1) {//Если враг один - развлекаемся с ним
			target=targets[0];
			targHips=target.GetComponent<MobController_2_1>().hips;
			return target;
		} else {//Другие враги таки есть
			//print("Enemy spotted");
			targets.Remove (lastTarget);
			target=targets[Random.Range(0,targets.Count)];
//			distToTarg=(target.transform.position-transform.position).magnitude;
			targHips=target.GetComponent<MobController_2_1>().hips;
			return target;
		}
	}*/
	GameObject GetTarget(){
		/*target = lastTarget;
		//distToTarg = 0;
		for (int i=0; i<interfaceControl.Instance.enemies.Count; i++) {
			tempTargPos=interfaceControl.Instance.enemies[i].transform.position;
			if(tempTargPos.z<0&&tempTargPos.x>-27&&tempTargPos.x<13){
				tempDist=(tempTargPos-transform.position).sqrMagnitude;
				if(target==lastTarget||tempDist<distToTarg){
					target=interfaceControl.Instance.enemies[i];
					distToTarg=tempDist;
				}
			}
		}
		return target;
		*/
		if (interfaceControl.Instance.enemies.Count == 0) {
			target = null;
			targHips=null;
		} else {
			target = interfaceControl.Instance.enemies [(Random.Range (0, interfaceControl.Instance.enemies.Count))];
			targHips=target.GetComponent<MobController_2_1>().hips;
		}
		return target;
	}
	void Activate(){
		StartCoroutine (Deploy ());
	}
	IEnumerator Deploy(){
		//было бы круто тут сделать спецэффект-раскидывание в начале поднятия
		rig.isKinematic = true;
		startPos = transform.position;
		endPos = transform.position + 3 * Vector3.up;
		startQ = transform.rotation;
		endQ = Quaternion.Euler (0, transform.rotation.eulerAngles.y, 0);
		for (int i=1; i<=15; i++) {
			transform.position=Vector3.Lerp(startPos,endPos,i/15f);
			transform.rotation=Quaternion.Lerp(startQ,endQ,i/15f);
			yield return new WaitForSeconds(quantTime);
		}
		rig.constraints = RigidbodyConstraints.FreezeRotationX|RigidbodyConstraints.FreezeRotationZ;
		rig.isKinematic = false;
		isActive = true;
		Deploing = true;
//		lastTarget = null;
		target = null;
	}
	IEnumerator Carmageddon(){
		while (isActive) {
			//GetTarget();
			if((target==null)||(target!=null&&target.activeSelf==false)){
				GetTarget();
				if(target==null){
					yield return new WaitForSeconds(smallCycleTime);
					continue;
				}
			}
			toTarget=targHips.position-transform.position;
			toTarget.y=0;
			startQ=transform.rotation;
			//endQ=Quaternion.Euler(0,Mathf.Clamp((Quaternion.LookRotation(toTarget)).eulerAngles.y,transform.rotation.y-rotCap,transform.rotation.y+rotCap),0);//работать не будет - угол цикличен
			endQ=Quaternion.LookRotation(toTarget);
			//startF=rig.velocity.magnitude;
			//endF=
			for(int i=1;i<=ticks;i++){
				transform.rotation=Quaternion.Lerp(startQ,endQ,i*2/ticks);
				//speed=Mathf.Clamp((rig.velocity.magnitude+Vector3.Dot(transform.forward,toTarget)*eAcc),0,forwCap);

				rig.velocity=transform.forward*forwCap;//*speed;
				yield return new WaitForSeconds(smallCycleTime);
			}

			//yield return new WaitForSeconds(cycleTime);
		}
	}
	void OnCollisionEnter(Collision col){
		if (Deploing) {
			Deploing=false;
			isActive=true;
			StartCoroutine(Carmageddon());
		}
		if (isActive) {
			MetaRigidbody tempMrig = MetaRigidbody.GetMetarig (col.gameObject);
			if (tempMrig != null && tempMrig.gameObject.tag == "enemy" && (target==null||target==tempMrig.gameObject)) {
//				lastTarget = tempMrig.gameObject;
				GetTarget();
			}
		}
	}
	void Deactivate(){
		StopAllCoroutines ();//остановите корутины, я сойду
		isActive = false; 
		Deploing = false;
		rig.constraints = RigidbodyConstraints.None;
		ObjectPool.instance.rezEndEffect.Spawn (transform).transform.parent=null;
	}
}
