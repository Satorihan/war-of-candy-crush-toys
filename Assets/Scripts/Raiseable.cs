﻿using UnityEngine;
using System.Collections;
using System;

public class Raiseable : MonoBehaviour {

	public Action MakeAlive, UnmakeAlive;
	public float lifetime=15;
	public float getupTime;
	public float cycleTime=1;
	//public int cost=1;
	Throwable throwController;

	// Use this for initialization
	void Start () {
		//Invoke ("Raise", 5);
		throwController = GetComponent<Throwable> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Z)) {
			Raise();
		}
		if (Input.GetKeyDown (KeyCode.X)) {
			Fall();
		}
	}
	public void Raise(){
		MakeAlive ();
		if (throwController != null)
			throwController.enabled = false;
		StartCoroutine (Mortality ());
	}
	public void Fall(){
		UnmakeAlive ();
		if (throwController != null)
			throwController.enabled = true;
	}
	IEnumerator Mortality(){
		yield return new WaitForSeconds(getupTime+lifetime);
		Fall ();
	}
}
