﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using AppodealAds.Unity.Common;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Android;

public class interfaceControl : MonoBehaviour {
	//public Dictionary<string,GameObject> dict;
	public static interfaceControl Instance;
	public int Level;
	//public int coin;
	int deltaCoin;
	public int candLeft;
	float energy;
	public Text TextCoin;
	public Text TextCandies;
	public Text EndCoin;
	public Text EndCandies;

	public Text TextLevel;
	/*
	public Text TextLevelEnd;
	*/
	RaycastHit hit;
	public GameObject StartPanel;
	public bool ShowStart;
	public bool ClearPlayerPrefs;
	public int SetLvl;

	public List<int> TimeLevel = new List<int>();
	public Slider TimeSlider;
	float time;//actually time;

	Camera cam;
	public Transform rezThing;

	public List<Candy> candies = new List<Candy>();
	public List<GameObject> enemies = new List<GameObject>();

//	public int StarLevel;

	//public List<Image> Star = new List<Image>();
	//public Sprite EnabledStar;
	//public Sprite DisabledStar;
	public MobSpawnControlScript scenrios;
	//public List<Sprite> energyFillSprite;
	public Image raiseButton;
	//public RectTransform heartDragger;
	public bool readyRaising = false;
	public bool waitForEnergy = false;

	public bool EndGame=false;

	[Header("Interface parts")]
	public GameObject shadowing;
	public GameObject pauseButton;
	public GameObject menuButton;
	public GameObject restartButton;
	public GameObject nextLevelButton;
	//public GameObject backButton;
	//public GameObject loadingLabel;
	public GameObject endPanel;
	public GameObject wonLabel;
	public GameObject lostLabel;
	//public GameObject yourPanel;
	public GameObject pauseBackground;
	public GameObject question;
	public TutorialHand tutorHand;

	//public AdUserCallback callbackCatcher=new AdUserCallback();


	void Awake()
	{
		Instance = this;
		//readyRaising = false;

	}
	void Start () 
	{
		EndGame = false;
		tutorHand.gameObject.SetActive (false);
		if(ClearPlayerPrefs)
		    PlayerPrefs.DeleteAll();
		deltaCoin = 0;

		if (SetLvl != 0)PlayerPrefs.SetInt("Level",SetLvl);;
		if(PlayerPrefs.GetInt("Level") == 0)
			PlayerPrefs.SetInt("Level",1);
		Level = PlayerPrefs.GetInt("Level");
		if (Level > 10)
			Level = 10;


		if(ShowStart){
			StartPanel.SetActive(true);
			TextLevel.text = "Level: " + Level;
			Invoke("DesabledStartPanel",2);
		}

		//coin = PlayerPrefs.GetInt("Coin");
		//TextCoin.text = "Coin: " + coin;
		TextCoin.text = "0";//coin.ToString();

		//candLeft=GameObject.FindGameObjectsWithTag ("candy").Length;
		candLeft = candies.Count;
		TextCandies.text = candLeft.ToString ();

		cam = Camera.main;
		energy = 0;
		raiseButton.fillAmount=0;
		//heartDragger.gameObject.SetActive(false);
		rezThing.gameObject.SetActive(false);

		InvokeRepeating("TimerLevel",2,1/15f);
		TimeSlider.maxValue = TimeLevel[Level-1]*15;
		scenrios.Do ();
		if (Level < 3)raiseButton.transform.parent.gameObject.SetActive(false);
		PauseGame ();//voodoo
		ResumeGame ();
		//ShowFullscreenAd.Init ();
		if (!UserAdvertise.inited) {
			UserAdvertise.inited = true;
			//AdUserCallback.OnRewardedVideoLoaded+=(()=>{Application.Quit();/*Debug.LogError("Rvideo loading complete");*/});
			//AdUserCallback.OnRewardedVideoFailedToLoad+=(()=>{UserAdvertise.ShowVideo ();/*Debug.LogError("Rvideo loading failed");*/});
			//AdUserCallback.OnRewardedVideoShown+=(()=>{Application.Quit();/*Debug.LogError("Rvideo shown;");*/});
			//AdUserCallback.OnVideoFailedToLoad+=(()=>{UserAdvertise.ShowFullScreenAd();});
			UserAdvertise.ShowBanner(AdType.BANNER_TOP);
		}

		//UserAdvertise.ShowBanner (AdType.BANNER_BOTTOM);
	}
	/*void test()
	{
		Debug.Log ("FAIL");
	}
	void untest()
	{
		Debug.Log ("SUCCESS");
	}*/


	public enum GameState
	{
		Normal,
		Pause,
		Won,
		Lost,
		Loading,
		Tutorial
	}

	public GameState GS;
	

	void Update () 
	{

//		s = Mathf.MoveTowards(s,i,0.005f);
		if(waitForEnergy&&energy>=1){
			//CGame.Instance.TutorialActivation(3);
			ActivateTutorial(3);
			waitForEnergy=false;
			return;
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (GS == GameState.Pause||GS==GameState.Tutorial) {
				//GS=GameState.Normal;
				ResumeGame();
			} else {
				if (GS == GameState.Lost || GS == GameState.Won) {
					//CGame.Instance.OnMenuButton_Click ();
					GotoMenu();
				} else {
					//CGame.Instance.OnPauseButton_Click ();
					PauseGame();
				}
			}
		}
		if (GS != GameState.Normal)
			return;
		if (readyRaising) {
			//heartDragger.position = Input.mousePosition;//не нравится мне это, но надо
			rezThing.position=cam.ScreenToWorldPoint(Input.mousePosition+Vector3.forward*8);
			if (Input.GetMouseButtonUp (0)) {
				readyRaising = false;
				//heartDragger.gameObject.SetActive (false);
				rezThing.gameObject.SetActive(false);
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray, out hit)) {
					GameObject hitTarget = hit.collider.gameObject;
					if(hitTarget.name=="Clickable")hitTarget=hitTarget.transform.parent.gameObject;
					MetaRigidbody tempRig = MetaRigidbody.GetMetarig (hitTarget);
					if (tempRig != null) {
						hitTarget=tempRig.gameObject;
					}
					Raiseable tempRa;
					if (tempRa = hitTarget.GetComponent<Raiseable> ()) {
						energy = 0;
						raiseButton.fillAmount = 0;
						tempRa.Raise ();
						ObjectPool.instance.RezSound.Spawn();
					}

				}
			}
		}
		//raiseDragger.position = Input.mousePosition;
	}
	public void TouchTheHeart(){
		if (energy >= 1) {
			readyRaising=true;
			//heartDragger.gameObject.SetActive(true);
			rezThing.gameObject.SetActive(true);
		}
	}
	void DesabledStartPanel()
	{
		StartPanel.SetActive(false);
	}
	void TimerLevel()
	{
		time++;
		TimeSlider.value = time;
		if(time == TimeLevel[Level-1]*15){
		   CancelInvoke("TimerLevel");
			Invoke("Won",2);
		}
	}
	void Won()
	{
		EndGame = true;
		GS = GameState.Won;
		HideInterface ();
		shadowing.SetActive (true);
		endPanel.SetActive (true);
		wonLabel.SetActive (true);
		question.SetActive (true);
		Time.timeScale = 0.5f;
		//CGame.Instance.OnWon();
		NextLevel();
		PlayerPrefs.SetInt("Coin",deltaCoin+PlayerPrefs.GetInt("Coin"));
		EndCandies.text = candLeft.ToString ();
		EndCoin.text = deltaCoin.ToString ();
	}
	void Lost(){
		EndGame = true;
		GS = GameState.Lost;
		HideInterface ();
		shadowing.SetActive (true);
		endPanel.SetActive (true);
		lostLabel.SetActive (true);
		restartButton.SetActive (true);
		menuButton.SetActive (true);
		Time.timeScale = 0.5f;
		//CGame.Instance.OnLoss ();
		PlayerPrefs.SetInt("Coin",deltaCoin+PlayerPrefs.GetInt("Coin"));
		//deltaCoin = 0;
		EndCandies.text = "0";
		EndCoin.text = deltaCoin.ToString();
	}
	public void CandyLost(){
		candLeft--;
		TextCandies.text = candLeft.ToString ();
		if (candLeft <= 0) {
			Lost();
		}
	}
	public void Coin(int c)
	{
		//coin += c;
		deltaCoin += c;
		//PlayerPrefs.SetInt("Coin",coin);
		//TextCoin.text = "Coin: " + coin;
		TextCoin.text = deltaCoin.ToString();
	}
	public void NextLevel()
	{
		Level += 1;
		PlayerPrefs.SetInt("Level",Level);

		if(Level > PlayerPrefs.GetInt("Max_Level"))
			PlayerPrefs.SetInt("Max_Level",Level);
	}
	public void Restrart()
	{
		//if(CGame.Instance.State == CGame.CGameState.Win)
		if (GS == GameState.Won)
		    Level -= 1;
		PlayerPrefs.SetInt("Level",Level);

		if(Level > PlayerPrefs.GetInt("Max_Level"))
			PlayerPrefs.SetInt("Max_Level",Level);
		//Application.LoadLevel("game");
		HideInterface ();
		Time.timeScale = 1;
		LoadingComponent.Instance.LoadNextLevel("game");
	}
	public void GetEnergy(){
		//if (energy < 10)energy++;
		//raiseButton.sprite = energyFillSprite [energy];
		if (energy < 1) {
			energy+=0.1f;
			raiseButton.fillAmount=energy;
		}
	}
	public void WatchVideoAgreed(){
		//watchVideo
		PlayerPrefs.SetInt("Coin",deltaCoin+PlayerPrefs.GetInt("Coin"));
		//PlayerPrefs.SetInt("Coin",deltaCoin+PlayerPrefs.GetInt("Coin"));
		deltaCoin *= 2;
		EndCoin.text = deltaCoin.ToString();

		ShowFullscreenAd.Show ();
		/*try{
			UserAdvertise.ShowRewardedVideo ();
		}
		catch{
			try{
				UserAdvertise.ShowVideo();
			}
			catch{
				UserAdvertise.ShowFullScreenAd();
			}
		}*/

		EndCoin.color = new Color (174 / 255f, 212 / 255f, 73 / 255f);
		question.SetActive(false);
		nextLevelButton.SetActive (true);
		menuButton.SetActive (true);
	}
	/*
	public void Raiser(){
		energy = 0;
		raiseButton.sprite=energyFillSprite[0];
	}*/
	void HideInterface(){
		pauseButton.SetActive (false);
		pauseBackground.SetActive (false);
		endPanel.SetActive (false);
		wonLabel.SetActive (false);
		lostLabel.SetActive (false);
		question.SetActive (false);
		nextLevelButton.SetActive (false);
		menuButton.SetActive (false);
		restartButton.SetActive (false);
		shadowing.SetActive (false);
		tutorHand.gameObject.SetActive (false);
	}
	public void PauseGame(){
		HideInterface ();
		shadowing.SetActive (true);
		pauseBackground.SetActive (true);
		GS = GameState.Pause;
		Time.timeScale = 0;
	}
	public void ResumeGame(){
		HideInterface ();
		pauseButton.SetActive (true);
		GS = GameState.Normal;
		Time.timeScale = 1;
	}
	public void ActivateTutorial(int tutNum){
		GS = GameState.Tutorial;
		Time.timeScale = 0;
		tutorHand.gameObject.SetActive (true);
		tutorHand.StartTutorial (tutNum);

	}
	public void GotoMenu(){
		//Application.LoadLevel ("menu");
		HideInterface ();
		Time.timeScale = 1;
		LoadingComponent.Instance.LoadNextLevel("menu");
	}
	public void NextLevelButton(){
		//Application.LoadLevel ("game");//conterintuitive
		HideInterface ();
		Time.timeScale = 1;
		LoadingComponent.Instance.LoadNextLevel("game");
	}
	public void ReturnCandy(Candy thatCandy){
		if(!candies.Contains(thatCandy))candies.Add (thatCandy);
	}
	public void AddEnemy(GameObject thatEnemy){
		if (thatEnemy.GetComponent<MobController_2_1> () && (!enemies.Contains (thatEnemy)))
			enemies.Add (thatEnemy);
	}
}
