﻿using UnityEngine;
using System.Collections;

public class Yang : MonoBehaviour {

	public float lifeTime=0.5f;
	public bool isSound = true;
	AudioSource it;
	// Use this for initialization

	void Awake () {
		it = GetComponent<AudioSource> ();
	}
	void OnEnable(){
		if(isSound)it.volume=(PauseController.isSoundEnabled?1:0);
		else it.volume=(PauseController.isMusicEnabled?1:0);
		Invoke ("End", lifeTime);
	}
	
	void End(){
		gameObject.Recycle ();
	}
}
