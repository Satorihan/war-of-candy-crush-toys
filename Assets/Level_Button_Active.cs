﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Level_Button_Active : MonoBehaviour {
	public enum ButtonState
	{
		Enabled,
		Disabled
	}
	
	ButtonState st;
	ButtonState state
	{
		get { return st; }
		set
		{
			st = value;
			switch (st)
			{
			case ButtonState.Enabled:
				ShowEnabled();
				break;
				
			case ButtonState.Disabled:
				ShowDisabled();
				break;
			}
		}
	}
	
	public ButtonState State
	{
		get { return state; }
	}
	Button itself;
	public Button button;
	public Sprite EnabledSprite;
	public Sprite DisabledSprite;
	public List<Image> Star = new List<Image>();
	public Sprite EnabledStar;
	public Sprite DisabledStar;

	public Image NewLevel;

	public int NamberStar;
	void Awake(){
		itself = GetComponent<Button> ();
	}
	void Start () 
	{

	}
	void Update () 
	{
	
	}
	void ShowEnabled()
	{
		//itself.enabled = true;
		itself.interactable = true;
		/*
		GetComponent<Image>().sprite = EnabledSprite;
		GetComponent<Button>().enabled = true;
		*/
		/*
		for(int i = 0; i < Star.Count; i++)
		{
			if(i+1 <= NamberStar){ 
			    Star[i].sprite = EnabledStar;
	        }else
				Star[i].sprite = DisabledStar;
		}
		
		if(NamberStar == 0)
			NewLevel.enabled = true;
		*/
	}
	void ShowDisabled()
	{
		//itself.enabled = false;
		itself.interactable = false;
		/*
		GetComponent<Image>().sprite = DisabledSprite;
		GetComponent<Button>().enabled = false;
		*/
		/*
		for(int i = 0; i < Star.Count; i++)
		{
			Star[i].sprite = DisabledStar;
		}
		*/
	}
	void DisableAllControls()
	{

	}
	public void OnEnabled(int star)
	{
		NamberStar = star;
		state = ButtonState.Enabled;
	}
	public void OnDisabled()
	{
		NamberStar = 0;
		state = ButtonState.Disabled;
	}
}
