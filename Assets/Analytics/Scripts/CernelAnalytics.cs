﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Analytics{
	public class CernelAnalytics : MonoBehaviour {
		void Awake ()
		{
			DontDestroyOnLoad (this.gameObject);
		}

		void Start ()
		{
			if(Application.loadedLevel == 0 && Application.loadedLevel < Application.levelCount){
				int currIndex = Application.loadedLevel;
				Application.LoadLevel (++currIndex);
			}
		}

		void Update ()
		{

		}
		#region sign-in methods
		void OnEnable()
		{
			EventManager.Instance.NewSceneEvent += StartNewScene;
			EventManager.Instance.PauseEvent += PauseApplication;
			EventManager.Instance.ResumeEvent += ResumeApplication;
			EventManager.Instance.OrientationChangeEvent += ChangeOrientation;
			EventManager.Instance.StartLevelEvent += StartLevel;
			EventManager.Instance.LevelWinEvent += WinLevel;
			EventManager.Instance.LevelLossEvent += LoseLevel;
			EventManager.Instance.ButtonClickEvent += ButtonClick;
			EventManager.Instance.GameOverEvent += GameOver;
			EventManager.Instance.ReportEventEmpty += ReportEvent;
			EventManager.Instance.ReportEventWithParams += ReportEventWithParams;
		}

		void OnDisable()
		{
			if (this.enabled) {
				EventManager.Instance.NewSceneEvent -= StartNewScene;
				EventManager.Instance.PauseEvent -= PauseApplication;
				EventManager.Instance.ResumeEvent -= ResumeApplication;
				EventManager.Instance.OrientationChangeEvent -= ChangeOrientation;
				EventManager.Instance.StartLevelEvent -= StartLevel;
				EventManager.Instance.LevelWinEvent -= WinLevel;
				EventManager.Instance.LevelLossEvent -= LoseLevel;
				EventManager.Instance.ButtonClickEvent -= ButtonClick;
				EventManager.Instance.GameOverEvent -= GameOver;
				EventManager.Instance.ReportEventEmpty -= ReportEvent;
				EventManager.Instance.ReportEventWithParams -= ReportEventWithParams;
			}
		}
		#endregion

		void PauseApplication (EventArgsData e){
			var _metrica = AppMetrica.Instance;
			_metrica.OnPauseApplication ();	
		}

		void ResumeApplication(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			_metrica.OnResumeApplication ();
		}

		void ApplicationInactive(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			_metrica.ReportEvent ("Application inactive");
		}

		void ChangeOrientation(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Orientation changed : " + e.String);
		}

		void StartLevel(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Level started : " + e.String);
		}

		void WinLevel(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Level passed : " + e.String);
		}

		void LoseLevel(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Level failed : " + e.String);
		}

		void GameLootTaked(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Loot taked : " + e.String + " count " + e.Int);
		}

		void GameLootSpent(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Loot spent : " + e.String + " count " + e.Int);
		}

		void StartNewScene(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Start scene : " + e.String);
		}

		void ButtonClick(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent ("Button clicked : " + e.String);
		}

		void GameOver(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			_metrica.ReportEvent ("Game Over");
		}

		void ReportEvent(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent (e.String);
		}

		void ReportEventWithParams(EventArgsData e){
			var _metrica = AppMetrica.Instance;
			if (e != null)
				_metrica.ReportEvent (e.String, e.Table);
		}

		void ReportEnvironmentValue(EventArgsData e){
			//var _metrica = AppMetrica.Instance;
			//if (e != null)
				//_metrica.SetEnvironmentValue (e.Table.,e.Table.Values[0]);
		}
	}
}

