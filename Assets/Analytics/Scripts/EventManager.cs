﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Класс для сбора статистики приложения
/// </summary>

namespace Analytics{

public delegate void EmptyEvent(EventArgsData e);
public delegate void GameObjectEvent();

public class EventManager : MonoBehaviour {
	/// <summary>
	/// Срабатывает при старте новой сцены	
	/// </summary>
	internal event EmptyEvent NewSceneEvent; 
	/// <summary>
	/// Отрабатывает при паузе
	/// </summary>
	internal event EmptyEvent PauseEvent;
	/// <summary>
	/// При продолжении
	/// </summary>
	internal event EmptyEvent ResumeEvent;
	/// <summary>
	/// Срабатывает при смене ориентации	
	/// </summary>
	internal event EmptyEvent OrientationChangeEvent;
	/// <summary>
	/// Срабатывает при старте уровня
	/// </summary>
	internal event EmptyEvent StartLevelEvent;
	/// <summary>
	/// Срабатывает при победе в уровне	
	/// </summary>
	internal event EmptyEvent LevelWinEvent;
	/// <summary>
	/// Срабатывает при проигрыше уровня
	/// </summary>
	internal event EmptyEvent LevelLossEvent;
	/// <summary>
	/// Срабатывает при получении внутриигровых бонусов
	/// </summary>
	internal event EmptyEvent BonusTakeEvent;
	/// <summary>
	/// Срабатывает при трате внуьтриигровых бонусов	
	/// </summary>
	internal event EmptyEvent BonusSpendEvent;
	/// <summary>
	/// Срабатывает при старте сцены	
	/// </summary>
	internal event EmptyEvent StartSceneEvent;
	/// <summary>
	/// Срабатывает при нажатии на кнопку
	/// </summary>
	internal event EmptyEvent ButtonClickEvent;
	/// <summary>
	/// Срабатывает при установке приложения	
	/// </summary>
	internal event EmptyEvent InstallApplicationEvent;
	/// <summary>
	/// Срабатываает при окнчании игры	
	/// </summary>
	internal event EmptyEvent GameOverEvent;
	/// <summary>
	/// Срабатывает при выходе из приложения	
	/// </summary>
	internal event EmptyEvent ApplicationQuitEvent;
	/// <summary>
	/// Срабатывает для кастомных ивентов без параметров
	/// </summary>
	internal event EmptyEvent ReportEventEmpty;
	/// <summary>
	/// Срабатывает для кастомных ивентов с параметрами
	/// </summary>
	internal event EmptyEvent ReportEventWithParams;

	
	public static EventManager Instance;

	string currentLevel = "";
	float currentTimeScale = 1.0f;
	DeviceOrientation currentOrientation;

	// Use this for initialization
	void Awake () {
		if(Instance == null){ 
			Instance = this;
		}
	}

	void Start(){
		
	}
	
	// Update is called once per frame
	void Update () {
		SetNewScene ();
		ProcessTime();
	}

	void ProcessTime ()
	{
		if (Time.timeScale == 0)
		{
			if (PauseEvent != null)
			{
				PauseEvent(new EventArgsData());
			}
			currentTimeScale = Time.timeScale;
		}
		else
		{
			if (currentTimeScale == 0)
			{
				currentTimeScale = Time.timeScale;
				if (ResumeEvent != null)
				{
					ResumeEvent(new EventArgsData());
				}
			}
			else
			{
				currentTimeScale = Time.timeScale;
			}
		}	
	}
	#region inner methods work in update
	void SetNewScene(){
			string level = Application.loadedLevelName;
			if (currentLevel != level) {
				currentLevel = level;

				StartNewScene (currentLevel);
			}
	}

	void InstallApplication ()
	{
		if(InstallApplicationEvent != null)
			InstallApplicationEvent(new EventArgsData());
	}

	void OnApplicationQuit(){
		if(ApplicationQuitEvent != null)
			ApplicationQuitEvent(new EventArgsData());
	}

	void ChangeOrientation ()
	{
		if (currentOrientation != Input.deviceOrientation) {
			currentOrientation = Input.deviceOrientation;
			if (OrientationChangeEvent != null)
				OrientationChangeEvent (new EventArgsData (currentOrientation.ToString ()));
		}
	}
	#endregion
	#region public methods
	public void StartNewScene(string sceneName)
	{
		if(NewSceneEvent != null)
			NewSceneEvent(new EventArgsData(sceneName));
	}
	public void StartLevel (string levelName)
	{
		if (!currentLevel.Equals (levelName)) {
			currentLevel = levelName;
		}
		if (StartLevelEvent != null)
			StartLevelEvent (new EventArgsData (levelName));
	}

	public void WinLevel (string levelName)
	{
		if (LevelWinEvent != null)
			LevelWinEvent (new EventArgsData (levelName));
	}

	public void LoseLevel (string levelName)
	{
		if (LevelLossEvent != null)
			LevelLossEvent (new EventArgsData (levelName));
	}

	public void TakeBonus (string bonusName, int count)
	{
		if(BonusTakeEvent != null)
			BonusTakeEvent(new EventArgsData(bonusName, count));
	}

	public void SpendBonus (string bonusName, int count)
	{
		if(BonusSpendEvent != null)
			BonusSpendEvent(new EventArgsData(bonusName, count));
	}

	public void StartScene (string sceneName)
	{
		if(StartSceneEvent != null)
			StartSceneEvent(new EventArgsData(sceneName));
	}

	public void ButtonClick (string buttonName)
	{
		if(ButtonClickEvent != null)
			ButtonClickEvent(new EventArgsData(buttonName));
	}

	public void ReportEvent(string message){
		if(ReportEventEmpty != null)
			ReportEventEmpty(new EventArgsData(message));	
	}

	public void ReportEventParams(string message, Hashtable values){
		if(ReportEventWithParams != null)
			ReportEventWithParams(new EventArgsData(message, values));	
	}
	#endregion
}

public class EventArgsData : System.EventArgs{
	public string String {get; internal set;}
	public int Int {get; internal set;}
	public Hashtable Table{ get; internal set;}

	public EventArgsData(){
		this.String = "";
		this.Int = 0;
		this.Table = null;
	}

	public EventArgsData(string str){
		this.String = str;
		this.Int = 0;
		this.Table = null;
	}

	public EventArgsData (string str, int num)
	{
		this.String = str;
		this.Int = num;
		this.Table = null;
	}
	
	public EventArgsData(string str, Hashtable values){
		this.String = str;
		this.Int = 0;
		this.Table = values;
	}
}
}


