﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Analytics;

/// <summary>
/// Class for using analytics
/// </summary>

public static class UserAnalytics
{
	#region methods for levels
	/// <summary>
	/// Start level "name"
	/// </summary>
	/// <param name="levelName">Level name.</param>
	static public void LevelStart(string levelName){
		if (EventManager.Instance)
			EventManager.Instance.StartLevel (levelName);
	}

	/// <summary>
	/// Win level "name"	
	/// </summary>
	/// <param name="levelName">Level name.</param>

	static public void LevelWin(string levelName){
		if (EventManager.Instance)
			EventManager.Instance.WinLevel(levelName);
	}
	/// <summary>
	/// Lose level "name"
	/// </summary>
	/// <param name="levelName">Level name.</param>
	static public void LevelLose(string levelName){
		if (EventManager.Instance)
			EventManager.Instance.LoseLevel (levelName);
	}
	#endregion
	#region methods for game states
	/// <summary>
	/// Get bonus named lootName count = "count", 
	/// </summary>
	/// <param name="lootName">Loot name.</param>
	/// <param name="count">Count.</param>
	static public void GameLootTake(string lootName, int count){
		if (EventManager.Instance)
			EventManager.Instance.TakeBonus (lootName, count);
	}
	/// <summary>
	/// Spend bonus named lootName count = "count"
	/// </summary>
	/// <param name="lootName">Loot name.</param>
	/// <param name="count">Count.</param>
	static public void GameLootSpent(string lootName, int count){
		if (EventManager.Instance)
			EventManager.Instance.SpendBonus (lootName, count);
	}
	/// <summary>
	/// Clicks the button named clickedButtonName
	/// </summary>
	/// <param name="clickedButtonName">Clicked button name.</param>
	static public void ClickButton(string clickedButtonName){
		if (EventManager.Instance)
			EventManager.Instance.ButtonClick (clickedButtonName);
	}
	#endregion
	#region push-notification
	/// <summary>
	/// Pushs the message sent.
	/// </summary>
	/// <param name="message">Message.</param>
	static public void PushMessageSent(string message){
		if (EventManager.Instance)
			EventManager.Instance.ReportEvent(message);
	}
	/// <summary>
	/// Pushs the message get.
	/// </summary>
	/// <param name="message">Message.</param>
	static public void PushMessageGet(string message){
		if (EventManager.Instance)
			EventManager.Instance.ReportEvent(message);
	}
	/// <summary>
	/// Pushs the message approved.
	/// </summary>
	/// <param name="message">Message.</param>
	static public void PushMessageApproved(string message){
		if (EventManager.Instance)
			EventManager.Instance.ReportEvent(message);
	}
	#endregion
	#region methods for custom params
	/// <summary>
	/// Reports the event with custom message
	/// </summary>
	/// <param name="message">Message.</param>
	static public void ReportEvent(string message){
		if (EventManager.Instance)
			EventManager.Instance.ReportEvent (message);
	}

//	static public void ReportEventWithParam<TKey,TValue>(string message, TKey key, TValue value){
//	
//	}
//
//	static public void ReportEventWithParams(string message, Hashtable values){
//		if (EventManager.Instance)
//			EventManager.Instance.ReportEventParams (message, values);
//	}
	#endregion
}

