﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

/// <summary>
/// Ad cernel.
/// This is a class for advertizing integrating in application
/// </summary>
namespace Advertising{
	public class AdCernel : MonoBehaviour  {
		static public AdCernel Instance;
		public string AppKey;
	
		// Use this for initialization
		void Awake(){
			if (!AppKey.Equals (""))
				Appodeal.initialize (AppKey,Appodeal.ALL);
			if (Instance == null)
				Instance = this;
		}

		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void ShowBanner(AdType type){
			switch (type) {
			case AdType.BANNER_BOTTOM:
				Appodeal.show (Appodeal.BANNER_BOTTOM);
				break;
			case AdType.BANNER_CENTER:
				Appodeal.show (Appodeal.BANNER_CENTER);
				break;
			case AdType.BANNER_TOP:
				Appodeal.show (Appodeal.BANNER_TOP);
				break;
			default:
				Appodeal.show (Appodeal.BANNER);
				break;
			}
		}

		public void ShowInterstitial(){
			Appodeal.show (Appodeal.INTERSTITIAL);
		}

		public void ShowRewardedVideo(){
			Appodeal.show (Appodeal.REWARDED_VIDEO);
		}
		public void ShowVideo(){
			Appodeal.setAutoCache(Appodeal.VIDEO, false);
			Appodeal.initialize(AppKey, Appodeal.ALL);
			Appodeal.cache(Appodeal.VIDEO);
			Appodeal.show (Appodeal.VIDEO);
		}

		public void HideAd(AdType type){
			switch (type) {
			case AdType.BANNER:
				Appodeal.hide (Appodeal.BANNER);
				break;
			case AdType.BANNER_BOTTOM:
				Appodeal.hide (Appodeal.BANNER_BOTTOM);
				break;
			case AdType.BANNER_CENTER:
				Appodeal.hide (Appodeal.BANNER_CENTER);
				break;
			case AdType.BANNER_TOP:
				Appodeal.hide (Appodeal.BANNER_TOP);
				break;
			case AdType.INTERSTITIAL:
				Appodeal.hide (Appodeal.INTERSTITIAL);
				break;
			case AdType.REWARDED_VIDEO:
				Appodeal.hide (Appodeal.REWARDED_VIDEO);
				break;
			case AdType.VIDEO:
				Appodeal.hide (Appodeal.VIDEO);
				break;
			}
		}
	}


}