﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnalyticsDemo : MonoBehaviour {
	public Text TimeScaleLabel;
	// Use this for initialization
	void Start () {
		UserAdvertise.Init ();
		AdUserCallback.OnBannerShown += PauseGame;
		AdUserCallback.OnBannerClicked += ResumeGame;
		AdUserCallback.OnBannerFailedToLoad += ResumeGame;
		//AdUserCallback.OnBannerClicked += Foo;
	}
	
	// Update is called once per frame
	void Update () {
		ChangeTimeScale ();
	}
//
//	public void ButtonClick(){
//		if (EventManager.Instance)
//			EventManager.Instance.ButtonClick ("Play");
//	}
//
//	public void LevelWin(){
//		if (EventManager.Instance)
//			EventManager.Instance.WinLevel (Application.loadedLevelName);	
//	}
//
//	public void LevelLose(){
//		if (EventManager.Instance)
//			EventManager.Instance.LoseLevel (Application.loadedLevelName);
//	}
//
	public void PauseGame(){
		Time.timeScale = 0;
	}

	public void ResumeGame(){
		Time.timeScale = 1.0f;
	}
//
//	void Foo(){
//	
//	}
//

	#region analytics demo
	public void ButtonClick(GameObject sender){
		UserAnalytics.ClickButton (sender.name);
	}

	public void StartLevel(){
		UserAnalytics.LevelStart ("Demo");
	}

	public void WinLevel(){
		UserAnalytics.LevelWin ("Demo");
	}

	public void LoseLevel(){
		UserAnalytics.LevelLose ("Demo");
	}

	public void ReportCustomEvent(){
		UserAnalytics.ReportEvent ("Reported message");
	}

	public void PushMessageSend(){
		UserAnalytics.PushMessageSent ("Push message");
	}
	#endregion

	#region advertise demo
	public void ShowBanner(){
		UserAdvertise.ShowBanner (AdType.BANNER);
	}

	public void ShowBannerMid(){
		UserAdvertise.ShowBanner (AdType.BANNER_CENTER);
	}

	public void ShowBannerTop(){
		UserAdvertise.ShowBanner (AdType.BANNER_TOP);
	}

	public void ShowBannerBottom(){
		UserAdvertise.ShowBanner (AdType.BANNER_BOTTOM);
	}

	public void ShowFullscreen(){
		UserAdvertise.ShowFullScreenAd ();
	}

	public void ShowVideo(){
		UserAdvertise.ShowVideo ();
	}
	public void HideAd(){
		UserAdvertise.HideCurrentAd();
	}

	public void ShowRewardedVideo(){
		UserAdvertise.ShowRewardedVideo();
	}
	#endregion

	void ChangeTimeScale(){
		TimeScaleLabel.text = Time.timeScale.ToString ();
	}
}