﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine.UI;

public class Level_Menu_Manager : MonoBehaviour {
	public List<int> Star = new List<int>();
	public List<Level_Button_Active> Button = new List<Level_Button_Active>();
	//public Button lvlSelectButton;
	public int Level;
	void Start () 
	{
		if(PlayerPrefs.GetInt("Max_Level")==0)
			PlayerPrefs.SetInt("Max_Level",1);
		//else
		Level = PlayerPrefs.GetInt("Max_Level");


		StarPrefs();

		for(int i = 0; i < Button.Count; i++)
		{
			if(i < Level)
			    Button[i].OnEnabled(Star[i]);
			else
			    Button[i].OnDisabled();
		}
	
	}

	void Update () 
	{
	
	}
	void StarPrefs()
	{
		/*
		Star[0] = PlayerPrefs.GetInt("Star_1");
		Star[1] = PlayerPrefs.GetInt("Star_2");
		Star[2] = PlayerPrefs.GetInt("Star_3");
		Star[3] = PlayerPrefs.GetInt("Star_4");
		Star[4] = PlayerPrefs.GetInt("Star_5");
		Star[5] = PlayerPrefs.GetInt("Star_6");
		Star[6] = PlayerPrefs.GetInt("Star_7");
		Star[7] = PlayerPrefs.GetInt("Star_8");
		Star[8] = PlayerPrefs.GetInt("Star_9");
		Star[9] = PlayerPrefs.GetInt("Star_10");
		*/
	}
	public void Play()
	{
		SaveLevel(PlayerPrefs.GetInt("Max_Level"));
	}
	public void SaveLevel(int L)
	{
		PlayerPrefs.SetInt("Level",L);
	}
}
