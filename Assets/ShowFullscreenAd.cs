﻿using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

public class ShowFullscreenAd {
	/*
	public static bool isInit=false;
	public static bool canRewarded=false;
	public static bool canVideo=false;

	public static void Init(){
		if (!isInit) {
			isInit=true;
			AdUserCallback.OnRewardedVideoLoaded += (()=>{canRewarded=true;});
			AdUserCallback.OnRewardedVideoShown+=(()=>{canRewarded=false;});
			AdUserCallback.OnVideoLoaded+=(()=>{canVideo=true;});
			AdUserCallback.OnVideoShown+=(()=>{canVideo=false;});
		}
	}
	*/
	public static void Show(){
		/*if (canRewarded)
			UserAdvertise.ShowRewardedVideo ();
		else if (canVideo)
			UserAdvertise.ShowVideo ();
		else
			UserAdvertise.ShowFullScreenAd ();
			*/
		if (Appodeal.isLoaded (Appodeal.REWARDED_VIDEO))UserAdvertise.ShowRewardedVideo ();
		else if (Appodeal.isLoaded (Appodeal.VIDEO))UserAdvertise.ShowVideo ();
		else UserAdvertise.ShowFullScreenAd();
	}
}
